﻿using System;

namespace WVSNancyAPI
{
	public class Employee
	{
		private string Firstname,Lastname,role; 

		public Employee ()
		{
			Firstname = "";
			Lastname = "";
			role = "undefined";

		}
		public Employee( string fname, string lname, string userrole ) {
		
			Firstname = fname;
			Lastname = lname;
			role = userrole;

		}
		public string getFirstname() {
			return this.Firstname;
		}
		public string getRole() {
			return this.role;	
		}
	}
}
