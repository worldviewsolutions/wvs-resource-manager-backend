﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Security.Principal;
namespace ResourceManager.Controllers
{
    public class globalData 
    {
       public const string authRole = "WVS\\Resource Managers";  //singular role now    
    }
    
    public class HomeController : Controller
    {

        [Authorize] // Require user to be on domain.
        public ActionResult Index()
        {
            string adminRole = globalData.authRole;
            ViewBag.auth = "";
            ViewBag.username = "";
        IPrincipal myPrincipal = this.User;
        if (!myPrincipal.IsInRole(adminRole))
        {
           ViewBag.auth = "Read Only";
          
        } else {
           ViewBag.auth = "Admin";
        }    
            ViewBag.Title = "Home Page";
            ViewBag.username = myPrincipal.Identity.Name;
            return View();
        }
    }
}
