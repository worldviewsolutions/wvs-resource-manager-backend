﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ResourceManager.Models;
using Task = ResourceManager.Models.Task;
using User = ResourceManager.Models.User;

namespace ResourceManager.Controllers
{
    public class ResourcesController : ApiController
    {
        private WVS_ProjectResourceMgmtAppEntities2 db = new WVS_ProjectResourceMgmtAppEntities2();

        // GET api/Resource
        [Authorize] // Require user to be in domain.
        public IQueryable<ResourceDTO> GetUsers()
        {
            var users = db.Users;
            List<ResourceDTO> updatedUsers = new List<ResourceDTO>();

            foreach (User user in users) {
                ResourceDTO newUser = new ResourceDTO();
                newUser.Name = user.LastName + ", " + user.FirstName;
                newUser.ID = user.Id;
                newUser.Roles = user.Roles;
                newUser.UserToTasks = user.UserToTasks;
                foreach (UserToTask relationship in user.UserToTasks) {
                    relationship.User = null;
                }
                foreach(Role role in user.Roles){
                    role.Users.Clear();
                }
                updatedUsers.Add(newUser);
            }

            return updatedUsers.AsQueryable();
        
    }

        // GET api/Resource/5
        [Authorize] // Require user to be in domain.
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetUser(int id)
        {
            User user = await db.Users.FindAsync(id);

            ResourceDTO newUser = new ResourceDTO();
            newUser.Name = user.LastName + ", " + user.FirstName;
            newUser.ID = user.Id;
            newUser.Roles = user.Roles;
            newUser.Tasks = new List<TaskDTO>();

            foreach (Role role in user.Roles) {
                role.Users.Clear();
            }

            foreach (UserToTask relationship in user.UserToTasks)
            {
                TaskDTO newTask = new TaskDTO();
                newTask.Name = relationship.Task.Name;
                newTask.ID = relationship.Task.Id;
                newTask.StartDate = relationship.Task.StartDate;
                newTask.EndDate = relationship.Task.EndDate;
                newTask.Comment = relationship.Task.Comment;
                newTask.Project = db.Projects.FirstOrDefault((p) => p.Id == relationship.Task.ProjectId);
                newTask.ProjectId = newTask.Project.Id;

                newTask.Users = new List<User>();

                newTask.Project.Tasks.Clear();
                newTask.Project.Milestones.Clear();
                newUser.Tasks.Add(newTask);
            }


            if (user == null)
            {
                return NotFound();
            }

            return Ok(newUser);
        }
  //      [Authorize(Roles = @"CONTOSO\VBmanagers,CONTOSO\CSmanagers")]
        //this needs to be rewritten if there is ever a way to change tasks when updating users
        // PUT api/Resource/5
        public async Task<IHttpActionResult> PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.Id)
            {
                return BadRequest();
            }

            User newUser = await db.Users.FindAsync(id);
            newUser.Roles.Clear();
            newUser.FirstName = user.FirstName;
            newUser.LastName = user.LastName;

            foreach (Role oldRole in user.Roles) {
                if (oldRole.Id != 0)
                {
                    Role dbRole = await db.Roles.FindAsync(oldRole.Id);
                    newUser.Roles.Add(dbRole);
                }
                else {
                    db.Roles.Add(oldRole);
                    newUser.Roles.Add(oldRole);
                }
            }

            db.Entry(newUser).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id)) 
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
  //      [Authorize(Roles = @"CONTOSO\VBmanagers,CONTOSO\CSmanagers")]
        //needs to be rewritten if there is a way to include tasks when adding resource
        //^^to update association table
        // POST api/Resource
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User newUser = new User();
            newUser.FirstName = user.FirstName;
            newUser.LastName = user.LastName;

            foreach (Role role in user.Roles) {
                if (role.Id != 0)
                {
                    Role newRole = await db.Roles.FindAsync(role.Id);
                    newUser.Roles.Add(newRole);
                }
                else {
                    newUser.Roles.Add(role);
                }
            };


            db.Users.Add(newUser);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = user.Id }, user);
        }
  //      [Authorize(Roles = @"CONTOSO\VBmanagers,CONTOSO\CSmanagers")]
        // DELETE api/Resource/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteUser(int id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            await db.SaveChangesAsync();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }
    }
}