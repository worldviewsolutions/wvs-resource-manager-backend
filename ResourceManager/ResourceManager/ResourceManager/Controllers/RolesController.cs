﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ResourceManager.Models;

namespace ResourceManager.Controllers
{
    public class RolesController : ApiController
    {
        private WVS_ProjectResourceMgmtAppEntities2 db = new WVS_ProjectResourceMgmtAppEntities2();

        // GET api/Roles
        [Authorize] // Require authenticated requests.
        public IQueryable<Role> GetRoles()
        {
            var roles = db.Roles;
            foreach(Role role in db.Roles){
                foreach (User user in role.Users) {
                    
                    user.Roles.Clear();
                    foreach(UserToTask relationship in user.UserToTasks){
                        relationship.User = null;
                        relationship.Task = null;
                    }
                }
            }

            return roles;
        }

        // GET api/Roles/5
        [Authorize] // Require user to be in domain.
        public async Task<IHttpActionResult> GetRole(int id)
        {
            Role role = await db.Roles.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }

            return Ok(role);
        }

        // PUT api/Roles/5
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        public async Task<IHttpActionResult> PutRole(int id, Role role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != role.Id)
            {
                return BadRequest();
            }

            db.Entry(role).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Roles
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        [ResponseType(typeof(Role))]
        public async Task<IHttpActionResult> PostRole(Role role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



            db.Roles.Add(role);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = role.Id }, role);
        }

        // DELETE api/Roles/5
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        [ResponseType(typeof(Role))]
        public async Task<IHttpActionResult> DeleteRole(int id)
        {
            Role role = await db.Roles.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }

            db.Roles.Remove(role);
            await db.SaveChangesAsync();

            return Ok(role);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RoleExists(int id)
        {
            return db.Roles.Count(e => e.Id == id) > 0;
        }
    }
}