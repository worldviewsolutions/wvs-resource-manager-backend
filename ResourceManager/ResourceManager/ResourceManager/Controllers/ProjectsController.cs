﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ResourceManager.Models;
using System.Data.Entity.SqlServer;



namespace ResourceManager.Controllers
{
   //IdentityBasicAuthentication] // Enable Basic authentication for this controller. 



    public class ProjectsController : ApiController
    {
        private WVS_ProjectResourceMgmtAppEntities2 db = new WVS_ProjectResourceMgmtAppEntities2();
        //private MockDbEntities db = new MockDbEntities();
       [Authorize] // Require authenticated requests.
        // GET api/Project
        public IQueryable<ProjectDTO> GetProjects()
        {
            var projects = db
                .Projects
                .Include("Task")
                .Select(t => new ProjectDTO
                {
                    ID = t.Id,
                    Name = t.Name,
                    StartDate = t.StartDate,
                    EndDate = t.EndDate,
                    Status = t.Status,
                    children = t.Tasks.Select(p => new TaskDTO
                    {
                        Name = p.Name,
                        ID = p.Id,
                        StartDate = p.StartDate,
                        EndDate = p.EndDate,
                        Comment = p.Comment,
                        UserToTasks = p.UserToTasks
                    })
                });



            foreach (ProjectDTO project in projects)
            {
                //foreach (TaskDTO task in project.children)
                //{
                //    foreach (User resource in task.Users) {
                //        //resource.Roles.Clear();
                //        resource.Tasks.Clear();
                //        foreach (Role role in resource.Roles) {
                //            role.Users.Clear();
                //        }
                //    }
                //}


            }


            return projects;
        }

        // GET api/Project/5
        [Authorize] // Require user to be in domain.
        [ResponseType(typeof(Project))]
        public async Task<IHttpActionResult> GetProject(int id)
        {
            //Project project = await db.Projects.FindAsync(id);
            var project = await db.Projects.Include(b => b.Tasks).Select(b =>
                new ProjectDTO()
                {
                    ID = b.Id,
                    Name = b.Name,
                    children = b.Tasks.Select(t => new TaskDTO
                    {
                        Name = t.Name,
                        ID = t.Id,
                        StartDate = t.StartDate,
                        EndDate = t.EndDate,
                    })
                }).SingleOrDefaultAsync(b => b.ID == id);

            if (project == null)
            {
                return NotFound();
            }

            return Ok(project);
        }

        // PUT api/Project/5
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        public async Task<IHttpActionResult> PutProject(int id, Project project)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != project.Id)
            {
                return BadRequest();
            }

            db.Entry(project).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        // POST api/Project
        [ResponseType(typeof(Project))]
        public async Task<IHttpActionResult> PostProject(Project project)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Projects.Add(project);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = project.Id }, project);
        }
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        // DELETE api/Project/5
        [ResponseType(typeof(Project))]
        public async Task<IHttpActionResult> DeleteProject(int id)
        {
            Project project = await db.Projects.FindAsync(id);
            if (project == null)
            {
                return NotFound();
            }

            db.Projects.Remove(project);
            await db.SaveChangesAsync();

            return Ok(project);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProjectExists(int id)
        {
            return db.Projects.Count(e => e.Id == id) > 0;
        }
    }
}