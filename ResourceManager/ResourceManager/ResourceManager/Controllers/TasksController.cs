﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ResourceManager.Models;

using Task = ResourceManager.Models.Task;
using User = ResourceManager.Models.User;
namespace ResourceManager.Controllers

{
    public class TasksController : ApiController 
    {
        private WVS_ProjectResourceMgmtAppEntities2 db = new WVS_ProjectResourceMgmtAppEntities2();
        [Authorize] // Require user to be in domain.
        // GET api/Tasks
        public IQueryable GetTasks()
        {
            var tasks = db.Tasks;
//
            List<TaskDTO> newTasks = new List<TaskDTO>();

            foreach (Task task in tasks)
            {
                TaskDTO newTask = new TaskDTO();
                newTask.Name = task.Name;
                newTask.ID = task.Id;
                newTask.StartDate = task.StartDate;
                newTask.EndDate = task.EndDate;
                //Users = t.Users, 
                newTask.Comment = task.Comment;
                newTask.Project = db.Projects.FirstOrDefault((p) => p.Id == task.ProjectId);
                newTask.Project.Milestones.Clear();
                newTask.Project.Tasks.Clear();
                newTask.UserToTasks = task.UserToTasks;
            }

            return tasks;
        }

        // GET api/Tasks/5
        [Authorize] // Require user to be in domain.
        [ResponseType(typeof(Task))]
        public async Task<IHttpActionResult> GetTask(int id)
        {

            Task task = await db.Tasks.FindAsync(id);
            TaskDTO newTask = new TaskDTO();
            newTask.Name = task.Name;
            newTask.ID = task.Id;
            newTask.StartDate = task.StartDate;
            newTask.EndDate = task.EndDate;
            //Users = t.Users, 
            newTask.Comment = task.Comment;
            newTask.Project = db.Projects.FirstOrDefault((p) => p.Id == task.ProjectId);
            newTask.Users = new List<User>();

            foreach (UserToTask relationship in task.UserToTasks)
            {
                relationship.User.Roles.Clear();
                relationship.User.UserToTasks.Clear();

                newTask.Users.Add(relationship.User);
            }
            //clear unneeded data
            newTask.Project.Tasks.Clear();

            newTask.Users.AsQueryable();

            if (task == null)
            {
                return NotFound();
            }

            return Ok(newTask);
        }
  //      [Authorize(Roles = @"CONTOSO\VBmanagers,CONTOSO\CSmanagers")]
        // PUT api/Tasks/5
         [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        public async Task<IHttpActionResult> PutTask(int id, TaskDTO updatedTask)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != updatedTask.ID)
            {
                return BadRequest();
            }

            Task oldTask = await db.Tasks.FindAsync(id);
            oldTask.UserToTasks.Clear();
            oldTask.UserToTasks = updatedTask.UserToTasks;
            

            oldTask.Name = updatedTask.Name;
            oldTask.StartDate = updatedTask.StartDate;
            oldTask.EndDate = updatedTask.EndDate;
            oldTask.Comment = updatedTask.Comment;
        //    oldTask.Project = db.Projects.FirstOrDefault((p) => p.Id == updatedTask.Project.Id);
          //  oldTask.ProjectId = updatedTask.Project.Id;

            //foreach (User userToTask in updatedTask.Users)
            //{
            //    UserToTask relationship = new UserToTask();
            //    relationship.TaskId = oldTask.Id;
            //    relationship.UserId = user.Id;
            //    relationship.Percentage = 
            //    db.UserToTasks.Add(relationship);
            //    oldTask.UserToTasks.Add(relationship);
            //}

            db.Entry(oldTask).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TaskExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Tasks
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        [ResponseType(typeof(Task))]
        public async Task<IHttpActionResult> PostTask(TaskDTO task)
        {


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState); 
            }

            var newTask = new Task();
            newTask.Name = task.Name;            
            newTask.StartDate = task.StartDate;
            newTask.EndDate = task.EndDate;
            newTask.Comment = task.Comment;
            //newTask.Project = db.Projects.FirstOrDefault((p) => p.Id == task.ProjectId);
            newTask.ProjectId = task.ProjectId;
           // newTask.LastModified = task.LastModified;
           
            db.Tasks.Add(newTask);
            await db.SaveChangesAsync();

            foreach (UserToTask user in task.UserToTasks)
            {
                UserToTask relationship = new UserToTask();
                relationship.TaskId = newTask.Id;
                relationship.UserId = user.UserId;
                relationship.Percentage = user.Percentage;
                db.UserToTasks.Add(relationship);
            };

            await db.SaveChangesAsync();

            

            return CreatedAtRoute("DefaultApi", new { id = newTask.Id }, newTask);
        }
         [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        // DELETE api/Tasks/5
        [ResponseType(typeof(Task))]
        public async Task<IHttpActionResult> DeleteTask(int id)
        {
            Task task = await db.Tasks.FindAsync(id);
            if (task == null)
            {
                return NotFound();
            }

            db.Tasks.Remove(task);
            await db.SaveChangesAsync();

            return Ok(task);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TaskExists(int id)
        {
            return db.Tasks.Count(e => e.Id == id) > 0;
        }
    }
}