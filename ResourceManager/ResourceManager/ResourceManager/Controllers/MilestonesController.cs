﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ResourceManager.Models;

namespace ResourceManager.Controllers
{
    

    public class MilestonesController : ApiController
    {
        private WVS_ProjectResourceMgmtAppEntities2 db = new WVS_ProjectResourceMgmtAppEntities2();

        // GET api/Milestone
        [Authorize]
        public IEnumerable<MilestoneDTO> GetMilestones()
        {
            var milestones = db
                .Milestones
                .Select(t => new MilestoneDTO
                {
                    ID = t.Id,
                    IsPaid = t.IsPaid,
                    StartTime = t.StartTime,
                    Date = t.Date,
                    IsHard = t.IsHard,
                    ProjectID = t.ProjectId,
                    Title = t.Title

                }).ToList();
            return milestones;
        }

        // GET api/Milestone/5
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        [ResponseType(typeof(Milestone))]
        public async Task<IHttpActionResult> GetMilestone(int id)
        {
            Milestone milestone = await db.Milestones.FindAsync(id);
            if (milestone == null)
            {
                return NotFound();
            }

            return Ok(milestone);
        }

  
        // PUT api/Milestone/5
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        public async Task<IHttpActionResult> PutMilestone(int id, Milestone milestone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != milestone.Id)
            {
                return BadRequest();
            }

            db.Entry(milestone).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MilestoneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
  //      [Authorize(Roles = @"CONTOSO\VBmanagers,CONTOSO\CSmanagers")]
        // POST api/Milestone
        
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        [ResponseType(typeof(Milestone))]
        public async Task<IHttpActionResult> PostMilestone(Milestone milestone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Milestones.Add(milestone);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = milestone.Id }, milestone);
        }
  //      [Authorize(Roles = @"CONTOSO\VBmanagers,CONTOSO\CSmanagers")]
        // DELETE api/Milestone/5
        [Authorize(Roles=globalData.authRole)] // Require authenticated requests.
        [ResponseType(typeof(Milestone))]
        public async Task<IHttpActionResult> DeleteMilestone(int id)
        {
            Milestone milestone = await db.Milestones.FindAsync(id);
            if (milestone == null)
            {
                return NotFound();
            }

            db.Milestones.Remove(milestone);
            await db.SaveChangesAsync();

            return Ok(milestone);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MilestoneExists(int id)
        {
            return db.Milestones.Count(e => e.Id == id) > 0;
        }
    }
}