﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResourceManager.Models
{
    public class ProjectDTO
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public string LastModified { get; set; }
        public string Status { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public IEnumerable<TaskDTO> children { get; set; }
        public virtual ICollection<Task> Tasks { get; set; } 

    }
}