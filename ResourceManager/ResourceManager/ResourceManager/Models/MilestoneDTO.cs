﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResourceManager.Models
{
    public class MilestoneDTO
    {
        public int ID { get; set; }
        public bool IsPaid { get; set; }
        public string StartTime { get; set; }
        public string Date { get; set; }
        public bool IsHard { get; set; }
        public int ProjectID { get; set; }
        public string Title { get; set; }
    }
}