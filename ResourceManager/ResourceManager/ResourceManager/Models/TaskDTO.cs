﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResourceManager.Models
{
    public class TaskDTO
    {
        public string Name { get; set; }
        public int ID { get; set; }
       // public int? Percentage { get; set; }
        public string StartDate { get; set; }
       // public string LastModified { get; set; }
        public string EndDate { get; set; }
       // public int UserID { get; set; }
        public string Comment { get; set; }
        public int ProjectId { get; set; }
        public  ICollection<UserToTask> UserToTasks { get; set; }
        public  ICollection<User> Users { get; set; }
        public virtual Project Project { get; set; }
    }
}