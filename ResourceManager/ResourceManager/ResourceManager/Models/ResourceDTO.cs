﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResourceManager.Models
{
    public class ResourceDTO
    {
        public string Name { get; set; }
        public int ID { get; set; }
        //public string Role { get; set; }

        public virtual ICollection<UserToTask> UserToTasks { get; set; }
        public virtual ICollection<TaskDTO> Tasks { get; set; }
        public virtual ICollection<Role> Roles { get; set; }

    }
}