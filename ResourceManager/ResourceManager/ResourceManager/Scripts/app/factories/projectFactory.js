﻿app.factory('projectFactory', ['$http', function ($http) {
    var urlBase = './api/projects';
    var projectFactory = {};

    projectFactory.getProjects = function () {
        return $http.get(urlBase);
    };

    projectFactory.getProject = function (id) {
        return $http.get(urlBase + '/' + id);
    };

    projectFactory.insertProject = function (project) {
        return $http.post(urlBase, project);
    };

    projectFactory.updateProject = function (project) {
        return $http.put(urlBase + '/' + project.Id, project)
    };

    projectFactory.deleteProject = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

    return projectFactory;

    
}]);
