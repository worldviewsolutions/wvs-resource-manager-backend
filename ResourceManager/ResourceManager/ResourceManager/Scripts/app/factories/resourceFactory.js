﻿app.factory('resourceFactory', ['$http', function ($http) {
    var urlBase = './api/resources';
    var resourceFactory = {};

    resourceFactory.getResources = function () {

        return $http.get(urlBase);
    };

    resourceFactory.getResource = function (id) {
        return $http.get(urlBase + '/' + id);
    };

    resourceFactory.insertResource = function (resource) {
        return $http.post(urlBase, resource);
    };

    resourceFactory.updateResource = function (resource) {
        return $http.put(urlBase + '/' + resource.Id, resource)
    };

    resourceFactory.deleteResource = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

    return resourceFactory;

}]);