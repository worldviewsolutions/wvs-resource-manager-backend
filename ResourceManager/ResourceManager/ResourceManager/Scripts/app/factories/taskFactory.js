﻿app.factory('taskFactory', ['$http', function ($http) {
    var urlBase = './api/tasks';
    var taskFactory = {};

    taskFactory.getTasks = function () {

        return $http.get(urlBase);
    };

    taskFactory.getTask = function (id) {
        return $http.get(urlBase + '/' + id);
    };

    taskFactory.insertTask = function (task) {
        return $http.post(urlBase, task);
    };

    taskFactory.updateTask = function (task) {
        return $http.put(urlBase + '/' + task.ID, task)
    };

    taskFactory.deleteTask = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

    return taskFactory;

}]);