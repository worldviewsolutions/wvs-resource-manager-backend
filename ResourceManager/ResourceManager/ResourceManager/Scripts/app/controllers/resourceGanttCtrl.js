﻿app.controller('resourceGanttCtrl', ['$scope', 'shared', function ($scope, shared) {
    var resourceDictionary = [];

    resourceGantt.init("resource_gantt");
    var usedIds = [];
   
    var resourceArrived = false;
    var taskArrived = false;
   
    $scope.$on('resources:updated',
        function (event, data) {
            resourceArrived = true;
            if (taskArrived && resourceArrived) {
                initResources();
            }
        });

    $scope.$on('events:updated',
        function (event, data) {
            taskArrived = true;
            if (taskArrived && resourceArrived) {
                initResources();
            }
        });

    //for multi bars on one line
    function createBox(sizes, class_name) {
        var box = document.createElement('div');
        box.style.cssText = [
            "height:" + sizes.height + "px",
            "line-height:" + sizes.height + "px",
            "width:" + sizes.width + "px",
            "top:" + sizes.top + 'px',
            "left:" + sizes.left + "px",
            "position:absolute"
        ].join(";");
        box.className = class_name;
        return box;
    }

    resourceGantt.templates.grid_row_class = resourceGantt.templates.task_class = function (start, end, task) {
        var css = [];
        if (resourceGantt.hasChild(task.id)) {
            css.push("task-parent");
            css.push("gantt_resource");
        }
        if (!task.$open && resourceGantt.hasChild(task.id)) {
            css.push("task-collapsed");
        }

        return css.join(" ");
    };

    resourceGantt.addTaskLayer(function show_hidden(task) {
        if (!task.$open && resourceGantt.hasChild(task.id)) {
            var sub_height = resourceGantt.config.row_height - 5,
                el = document.createElement('div'),
                sizes = resourceGantt.getTaskPosition(task);

            var sub_tasks = resourceGantt.getChildren(task.id);

            var child_el;

            for (var i = 0; i < sub_tasks.length; i++) {
                var child = resourceGantt.getTask(sub_tasks[i]);
                var child_sizes = resourceGantt.getTaskPosition(child);


                //console.log(child.text);
                if (child.text === "CONTENTION") {
                    child_el = createBox({
                        height: sub_height,
                        top: sizes.top,
                        left: child_sizes.left,
                        width: child_sizes.width
                    }, "gantt_task_line contention");
                    }
                else
                    child_el = createBox({
                        height: sub_height,
                        top: sizes.top,
                        left: child_sizes.left,
                        width: child_sizes.width
                    }, "resource_preview gantt_task_line");
                // child_el.innerHTML = child.text;
                el.appendChild(child_el);
            }
            return el;
        }
        return false;
    });
    
    getUniqueResourceId1 = function (id) {
        for (var i = 0; i < resourceDictionary.length; i++) {
            if (id == resourceDictionary[i].ganttId) {
                id++;
                i = 0;
            }
        }
        return id;
    }

    getTask1 = function (id) {
        for (var i = 0; i < shared.tasks.length; i++) {
            if (shared.tasks[i].Id == id) {
                return shared.tasks[i];
            }
        }
        return null;
    }
  
    initResources = function () {
        var data = [];
        resourceDictionary = [];
        var startDate=null,
            endDate=null;
        for (var i = 0; i < shared.resources.length; i++) {
            var resource = shared.resources[i];
            var resourceItem = {
                text: resource.Name,
                first: resource.Name.split(", ")[1],
                last: resource.Name.split(", ")[0],
                start_date: "4-7-2014",
                duration: 1, 
                id: getUniqueResourceId1(resource.ID),
                type: "project",
                roles:[]
            }
            for (index in resource.Roles) {
                resourceItem.roles.push(resource.Roles[index].Id);
            }
            resourceDictionary.push({ dbId: resource.ID, ganttId: resourceItem.id });
            usedIds.push(resourceItem.id);
            data.push(resourceItem);
            
            for (var j = 0; j < resource.UserToTasks.length; j++) {
                var task = getTask1(resource.UserToTasks[j].TaskId);
                if (task != null) {
                    if (!startDate) {
                        startDate = task.StartDate;
                        endDate = task.EndDate;
                    }
                    else {
                        if (new Date(startDate) > new Date(task.StartDate))
                            startDate = task.StartDate;
                        if (new Date(endDate) < new Date(task.EndDate))
                            endDate = task.EndDate;
                    }
                    var item = {
                        text: task.Name,
                        start_date: task.StartDate,
                        end_date: task.EndDate,
                        id: getUniqueResourceId1(task.Id),
                        parent: resourceItem.id,
                        percent: resource.UserToTasks[j].Percentage,
                        type: gantt.config.types.task,
                        comment:task.Comment
                    }
                    resourceDictionary.push({ dbId: task.Id, ganttId: item.id })
                    data.push(item);
                }
            }

            var contentionStart = null,
                contentionEnd = null;
            var allocation = 0;
            for (k = new Date(startDate) ; k < new Date(endDate) ; k.setDate(k.getDate() + 1)) {
                allocation = 0;
                for (var j = 0; j < resource.UserToTasks.length; j++) {
                    var task = getTask1(resource.UserToTasks[j].TaskId);
                    if (task != null && new Date(task.StartDate) <= k && k <= new Date(task.EndDate)) {
                        allocation = allocation + resource.UserToTasks[j].Percentage;
                    }
                }
                if (allocation >= 100) {
                    if (contentionStart == null) {
                        contentionStart = k.toLocaleDateString();
                    }
                    contentionEnd = k.toLocaleDateString();
                }
                else {
                    if (contentionStart) {
                        var contentionItem = {
                            text: "CONTENTION",
                            start_date: contentionStart,
                            end_date: contentionEnd,
                            id: getUniqueResourceId1(task.Id*10),
                            parent: resourceItem.id,
                            percent: 100,
                            type: gantt.config.types.task,
                            color: "red"
                        }
                        data.push(contentionItem);
                    }
                    contentionStart = null;
                    contentionEnd = null;
                }
            }
            if (allocation >= 100) {
                var contentionItem = {
                    text: "CONTENTION",
                    start_date: contentionStart,
                    end_date: k.toLocaleDateString(),
                    id: getUniqueResourceId1(task.Id*10),
                    parent: resourceItem.id,
                    percent: 100,
                    type: gantt.config.types.task,
                    color: "red"
                }
                contentionStart = null;
                contentionEnd = null;
                data.push(contentionItem);
            }
            startDate = null;
            endDate = null;
        }

        var formattedData = JSON.stringify({ data: data });
        resourceGantt.clearAll();
        resourceGantt.parse(formattedData);

        shared.masterKeys.push({ view: "resourceGantt", keys: usedIds })
    }

    getDatabaseId = function (id) {
        for (var i = 0; i < resourceDictionary.length; i++)
            if (id == resourceDictionary[i].ganttId)
                return resourceDictionary[i].dbId;
    }

    /*--------------Configure Preview---------------*/
    $scope.ChangedItems = [];
    $scope.DeletedItems = [];
    $scope.AddedItems = [];

    $scope.isPreviewing = shared.isPreviewing;
    $scope.$on('preview:updated', function (event, data) { $scope.togglePreview(data[0],data[1]);});

    $scope.togglePreview = function (isPreviewing,confirm) {
        $scope.isPreviewing = isPreviewing;
        if (!isPreviewing) {//If being set from true to false
            if (confirm) {
                for (var items in $scope.AddedItems) {
                    item = $scope.AddedItems[items];
                    item.text = item.last + ", " + item.first;
                    var resource = {
                        "FirstName": item.first,
                        "LastName": item.last,
                        "Roles": [],
                    }
                    for (var i = 0; i < item.roles.length; i++)
                        for (var j = 0; j < shared.roles.length; j++)
                            if (shared.roles[j].Id == item.roles[i])
                                resource.Roles.push(shared.roles[j]);
                    shared.addResource(resource);
                }
                for (var items in $scope.ChangedItems) {
                    item = $scope.ChangedItems[items];
                    if (item.type == "task") {
                        item.id = getDatabaseId(item.id);
                        var task = getTask1(item.id);
                        item.UserToTasks = task.UserToTasks;
                        for (var i = 0; i < item.UserToTasks.length; i++)
                            if (item.UserToTasks[i].UserId == item.parent)
                                item.UserToTasks[i].Percentage = parseInt(item.percent);
                        item.parent = task.ProjectId;
                        shared.updateTask(item);
                    }
                    else if (item.type == "project") {
                        var resource = {};
                        for (var i = 0; i < shared.resources.length; i++) {
                            if (getDatabaseId(item.id) == shared.resources[i].ID) {
                                resource = shared.resources[i];
                                break;
                            }
                        }
                        resource.Name = item.last + ", " + item.first;
                        resource.Id = resource.ID;
                        resource.Roles = [];
                        for (var i = 0; i < item.roles.length; i++)
                            for (var j = 0; j < shared.roles.length; j++) {
                                if (shared.roles[j].Id == item.roles[i])
                                    resource.Roles.push(shared.roles[j]);
                            }
                        shared.updateResource(resource);
                    }
                }
                for (var items in $scope.DeletedItems) {
                    item = $scope.DeletedItems[items];
                    shared.deleteTask(item);
                }
            }
            else {
                shared.getInitialData();
            }
            $scope.ChangedItems = [];
            $scope.AddedItems = [];
            $scope.DeletedItems = [];
        }
    };
    /*--------------Configure Preview---------------*/

    //Design roles edit field for resource lightbox
    $scope.$on('roles:updated', function (event, data) {
        //Create custom array for lightbox
        opts = [];
        for (var i = 0; i < data.length; i++) {
            opts.push({ key: data[i].Id, label: data[i].Name });
        }

        resourceGantt.form_blocks["my_editor"] = {
            render: function (sns) {
                var str = "<div class='dhx_cal_ltext' style='height:60px;'><select id=\"SelectRole\" size=\"4\" multiple>";
                for (index in opts)
                    str += "<option value=" + opts[index].key + ">" + opts[index].label + "</option>";
                str += "</select></div>";
                return str;
            },
            set_value: function (node, value, task, section) {
                var options = document.getElementById("SelectRole").options;
                if (task.roles)
                    for (index in options) {
                        if (task.roles.indexOf(parseInt(options[index].value)) > -1)
                            options[index].selected = true;
                        else
                            options[index].selected = false;
                    }
                else
                    for (index in options)
                        options[index].selected = false;
            },
            get_value: function (node, task, section) {
                var selected = [];
                var options = document.getElementById("SelectRole").options;

                for (index in options) {
                    if (options[index].selected == true) {
                        selected.push(parseInt(options[index].value));
                    }
                };
                return selected;
            }
        };
    });

    /*-----------Attatch Functions to Gantt Events----------*/

    //Updates projects as resources and tasks as tasks
    resourceGantt.attachEvent("onAfterTaskUpdate", function (id, item) {
        if ($scope.isPreviewing) { $scope.ChangedItems.push(item); }
        else {
            if (item.type == "task") {
                item.id = getDatabaseId(id);
                var task = getTask1(item.id);
                item.UserToTasks = task.UserToTasks;
                for (var i = 0; i < item.UserToTasks.length; i++)
                    if (item.UserToTasks[i].UserId == item.parent)
                        item.UserToTasks[i].Percentage = parseInt(item.percent);
                item.parent = task.ProjectId;
                shared.updateTask(item);
            }
            else if (item.type == "project") {
                var resource = {};
                for (var i = 0; i < shared.resources.length; i++) {
                    if (getDatabaseId(id) == shared.resources[i].ID) {
                        resource = shared.resources[i];
                        break;
                    }
                }
                resource.Name = item.last + ", " + item.first;
                resource.Id = resource.ID;
                resource.Roles = [];
                for (var i = 0; i < item.roles.length; i++)
                    for (var j = 0; j < shared.roles.length; j++) {
                        if (shared.roles[j].Id == item.roles[i])
                            resource.Roles.push(shared.roles[j]);
                    }
                shared.updateResource(resource);
            }
        }
    });
    //Tasks cannot be added only resources
    resourceGantt.attachEvent("onAfterTaskAdd", function (id, item) {
        if ($scope.isPreviewing) { $scope.AddedItems.push(item); }
        else {
            item.text = item.last + ", " + item.first;
            var resource = {
                "FirstName": item.first,
                "LastName": item.last,
                "Roles": [],
            }
            for (var i = 0; i < item.roles.length; i++)
                for (var j = 0; j < shared.roles.length; j++)
                    if (shared.roles[j].Id == item.roles[i])
                        resource.Roles.push(shared.roles[j]);
            shared.addResource(resource);
        }
    });
    //Resources cannot be delted only tasks
    resourceGantt.attachEvent("onBeforeTaskDelete", function (id, item) {
        if (item.type == "project")
            return false;
        else if ($scope.isPreviewing) { $scope.DeletedItems.push(item); return true;}
        else{
            shared.deleteTask(item);
            return true;
        }
    });

    //Can only add resources
    resourceGantt.attachEvent('onTaskCreated', function (task) {
        if (!task.parent) {//Adding a resource
            task.type = "project";
            return true;
        }
        else//Adding a task
            return false;
    });

    /*-----------Attatch Functions to Gantt Events----------*/
}]);