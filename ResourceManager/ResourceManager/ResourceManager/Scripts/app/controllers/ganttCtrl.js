﻿app.controller('ganttCtrl', ['$scope', 'shared', function ($scope, shared) {
    $scope.tasks = [];
    var opts = [];

    projectGantt.init("project_gantt");

    $scope.$on('dataReady', function (event, args) {
        var tasks = args[0],
            projects = args[1],
            milestones = args[2];
            
        var data = [],
            usedIds = [];

        //get all the project ids for comparison
        for (var i = 0; i < projects.length; i++)
            usedIds.push(projects[i].ID);

        shared.masterKeys.push({view: "projectGantt", keys: usedIds}) //keep a dictionary of all the root items

        shared.idDictionary = [];
        //push projects in the correct format that the timeline view needs
        for (var i = 0; i < projects.length; i++) {
            //shared.projects.push(projects[i]);
            var oldProject = projects[i];
                            
            var project = {
                "id": oldProject.ID,
                "text": oldProject.Name,
                "start_date": oldProject.StartDate,
                "end_date": oldProject.EndDate,
                "projectStatus": oldProject.Status,
                "type": gantt.config.types.project
            };

            data.push(project);

            //if the project has a child, put it in the correct format for timeline view and push to array
            if (oldProject.children.length != 0) {
                for (var j = 0; j < oldProject.children.length; j++) {
                    var oldTask = oldProject.children[j];
                    //attach users to resources in gantt view
                    for (var index1 = 0; index1 < tasks.length; index1++) {
                        if (oldTask.ID == tasks[index1].ID) {
                            oldProject.children[j] = tasks[index1];
                        }
                    }

                    var oldTask = oldProject.children[j];
                    var oldId = oldTask.ID;

                    while (usedIds.indexOf(oldId) > -1) {
                        oldId++;
                    };

                    usedIds.push(oldId);

                    shared.idDictionary.push({
                        "dataBaseId": oldTask.ID,
                        "ganttId": oldId
                    });

                    var users = [];
                    for (index in oldTask.UserToTasks)
                        users.push(oldTask.UserToTasks[index].UserId);

                    var ganttTask = {
                        "id": oldId,
                        "text": oldTask.Name,
                        "start_date": oldTask.StartDate,
                        "end_date": oldTask.EndDate,
                        "parent": oldProject.ID,
                        "comment": oldTask.Comment,
                        "type": gantt.config.types.task,
                        "project": oldTask.Project,
                        "users": users,
                        "userToTasks": oldTask.UserToTasks
                    };
                    //needed for updates to  work
                    if (ganttTask.comment == null) {
                        ganttTask.comment = "";
                    }

                    data.push(ganttTask);
                };
            }
        };

        //get initial milestone data
        for (var i = 0; i < milestones.length; i++) {
            var oldMilestone = milestones[i],
                oldId = milestones[i].ID;
            //check the used id array to see if id needs to be 
            //adjusted for gantt chart 
            while (usedIds.indexOf(oldId) > -1) {
                oldId++;
            };

            usedIds.push(oldId);

            //update dictionary
            shared.idDictionary.push({
                "dataBaseId": oldMilestone.ID,
                "ganttId": oldId
            });

            //update the gantt array
            var ganttMilestone = {
                "id": oldId,
                "IsPaid": oldMilestone.IsPaid,
                "start_date": oldMilestone.StartTime,
                //"Date": oldMilestone.Date,
                "IsHard": oldMilestone.IsHard,
                "parent": oldMilestone.ProjectID,
                "text": oldMilestone.Title,
                "type": gantt.config.types.milestone,
                "color": setMilestoneColor(oldMilestone.IsHard, oldMilestone.IsPaid)
            };
            data.push(ganttMilestone);
        };
        $scope.tasks = JSON.stringify({ data: data });
        projectGantt.clearAll();
        projectGantt.parse($scope.tasks)

    });
    $scope.$on('resources:updated', function (event, data) {
        //Create custom array for lightbox
        opts = [];
        for (var i = 0; i < data.length; i++) {
            opts.push({ key: data[i].ID, label: data[i].Name });
        }
   
        projectGantt.form_blocks["my_editor"] = {
            render: function (sns) {
                var str = "<div class='dhx_cal_ltext' style='height:60px;'><select id=\"mySelect\" size=\"4\" multiple>";
                for (index in opts)
                    str += "<option value=" + opts[index].key + ">" + opts[index].label + "</option>";
                str += "</select></div>";
                return str;
            },
            set_value: function (node, value, task, section) {
                var options = document.getElementById("mySelect").options;
                if (task.users)
                    for (index in options) {
                        if (task.users.indexOf(parseInt(options[index].value)) > -1)
                            options[index].selected = true;
                        else
                            options[index].selected = false;
                    }
                else
                    for (index in options)
                        options[index].selected = false;
            },
            get_value: function (node, task, section) {
                var selected = [];
                var options = document.getElementById("mySelect").options;

                for (index in options) {
                    if (options[index].selected == true) {
                        selected.push(parseInt(options[index].value));
                    }
                };
                return selected;
            }
        };
    });
   
    //for multi bars on one line
    function createBox(sizes, class_name) {
        var box = document.createElement('div');
        box.style.cssText = [
            "height:" + sizes.height + "px",
            "line-height:" + sizes.height + "px",
            "width:" + sizes.width + "px",
            "top:" + sizes.top + 'px',
            "left:" + sizes.left + "px",
            "position:absolute"
        ].join(";");
        box.className = class_name;
        return box;
    }

    projectGantt.templates.grid_row_class = projectGantt.templates.task_class = function (start, end, task) {
        var css = [];
        if (projectGantt.hasChild(task.id)) {
            css.push("task-parent");
        }
        if (!task.$open && projectGantt.hasChild(task.id)) {
            css.push("task-collapsed");
        }

        return css.join(" ");
    };

    projectGantt.addTaskLayer(function show_hidden(task) {
        if (!task.$open && projectGantt.hasChild(task.id)) {
            var sub_height = projectGantt.config.row_height - 5,
                el = document.createElement('div'),
                sizes = projectGantt.getTaskPosition(task);

            var sub_tasks = projectGantt.getChildren(task.id);

            var child_el;

            for (var i = 0; i < sub_tasks.length; i++) {
                var child = projectGantt.getTask(sub_tasks[i]);
                var child_sizes = projectGantt.getTaskPosition(child);

                child_el = createBox({
                    height: sub_height,
                    top: sizes.top,
                    left: child_sizes.left,
                    width: child_sizes.width
                }, "child_preview gantt_task_line");
               // child_el.innerHTML = child.text;
                el.appendChild(child_el);
            }
            return el;
        }
        return false;
    });

    /*--------------Configure Preview---------------*/

    $scope.ChangedItems = [];
    $scope.DeletedItems = [];
    $scope.AddedItems = [];

    $scope.isPreviewing = shared.isPreviewing;
    $scope.$on('preview:updated', function (event, data) { $scope.togglePreview(data[0], data[1]); });

    $scope.togglePreview = function (isPreviewing, confirm) {
        $scope.isPreviewing = isPreviewing;
        if (!isPreviewing) {//If being set from true to false
            if (confirm) {
                for (var items in $scope.AddedItems) {
                    item = $scope.AddedItems[items];
                    if (item.type == "task")//Add task
                        shared.addTask(item);
                    else if (item.type == "project")//Add project
                        shared.addProject(item);
                    else if (item.type == "milestone")//Add milestone //TODO: Fix insertmilestone
                        shared.addMilestone(item);
                }
                for (var items in $scope.ChangedItems) {
                    item = $scope.ChangedItems[items];
                    if (item.type == "task") {//updated task
                        shared.updateTask(item);
                    }
                    else if (item.type == "project")//updated project
                        shared.updateProject(item);
                    else if (item.type == "milestone") {//updated milestone
                        item.color = setMilestoneColor(item.IsHard, item.IsPaid);
                        shared.updateMilestone(item);
                    }
                }
                for (var items in $scope.DeletedItems) {
                    item = $scope.DeletedItems[items];
                    shared.deleteTask(item);
                }
            }
            else {
                shared.getInitialData();
            }
            $scope.ChangedItems = [];
            $scope.AddedItems = [];
            $scope.DeletedItems = [];
        }
    };

    /*--------------Configure Preview---------------*/

    /*-----------Attatch Functions to Gantt Events----------*/

    //After Item is added in timelime call update function
    projectGantt.attachEvent('onAfterTaskUpdate', function (id, item) {
        //After Item is updated in timeline call update function
        if ($scope.isPreviewing) { $scope.ChangedItems.push(item); }
        else {
            if (item.type == 'task') {//updated task
                shared.waitingToUpdate = item;
                //shared.updateTask(item);
            }
            else if (item.type == 'project')//updated project
                shared.updateProject(item);
            else if (item.type == 'milestone') {//updated milestone
                shared.updateMilestone(item);
            }
        }
    });
    //After Item is added in timelime call add function
    projectGantt.attachEvent('onAfterTaskAdd', function (id, item) {
        if ($scope.isPreviewing) { $scope.AddedItems.push(item); }
        else {
            if (item.type == 'task')//Add task
                if (item.parent == 0)
                    window.alert("Set type to project");
                else
                    //shared.addTask(item);
                    shared.waitingToAdd = item;
            else if (item.type == 'project')//Add project
                shared.addProject(item);
            else if (item.type == 'milestone')//Add milestone //TODO: Fix insertmilestone
                shared.addMilestone(item);
        }
    });
    //After Item is delted in timeline delete from database
    projectGantt.attachEvent('onAfterTaskDelete', function (id, item) {
        if ($scope.isPreviewing) { $scope.DeletedItems.push(item); }
        else {
            shared.deleteTask(item);
        }
    });

    //After task is update via lightbox will prompt user for individual user to task percentages
    //Using generated modal boxs
    projectGantt.attachEvent('onLightBoxSave', function (id, item, is_new) {
        var task = elementByIdArray(shared.getDataBaseId(id), shared.tasks);
        if (item.type == 'task' && item.users.length > 0) {
            var str = "",
                user = "";

            for (var i = 0; i < item.users.length; i++) {
                for (var j = 0; j < opts.length; j++) {
                    if (opts[j].key == item.users[i]) {
                        user = opts[j].label;
                        break;
                    };
                }

                var percent = 0;
                if (!task) { }
                else {
                    for (var k = 0; k < task.UserToTasks.length; k++) {
                        if (task.UserToTasks[k].UserId == opts[j].key)
                            percent = task.UserToTasks[k].Percentage;
                    }
                }

                str = str + "<div><label>" + "Resource: " + user + " - Set Percentage" + "<input class=\"percents\" id=" + item.users[i] + " type=\"text\" value=\"" + percent + "\"/>" + "</label></div>" + "\n";
            }

            var footer = "<div class=\"modal-footer\">" +
                            "<button type=\"submit\" class=\"btn btn-primary\">" +
                            "Continue" +
                            "</button>" +
                        "</div>";
            str = str + footer;

            $('#taskPercentHeader').html(
                    "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">" +
                    "<span aria-hidden=\"true\">&times;</span>" +
                    "</button>" +
                        "<h4 class=\"modal-title\" id=\"myModalLabel\">" +
                        "Set User To Task Percentages for Task: " +
                        item.text +
                        "</h4>"
                );

            if (is_new)
                $('#taskAddPercentForm').html(str);
            else
                $('#taskPercentForm').html(str);

            $('#updateUserToTaskPercent').modal('show');
        }
        else if (item.type == 'task'){
            item.UserToTasks = [];
            if (is_new)
                shared.addTask(item);
            else
                shared.updateTask(item);
        }
            return true;
    });

    //If adding new gantt Task, if not child of project then defaults to project in lightbox
    projectGantt.attachEvent('onTaskCreated', function (task) {
            if (!task.parent)
                task.type = "project";
            return true;
        });

    /*-----------Attatch Functions to Gantt Events----------*/

    $scope.hideDateFields = function () {
        shared.hideDateFields();
    };

    function elementByIdArray(id, array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].Id == id)
                return array[i];
        }
        return null;
    };

    $scope.debug = function () {
    }; 
}]);