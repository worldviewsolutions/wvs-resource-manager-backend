﻿app.controller('projectResourceGanttCtrl', ['$scope', 'shared', function ($scope, shared) {
    var data = [];
    var formattedData = [];
    var projectResourceDictionary = [];
    var usedIds = [];

    projectResourceGantt.init("project_resource_gantt");

    var resourceArrived = false;
    var projectsArrived = false;

    $scope.$on('resources:updated',
        function (event, data) {
            resourceArrived = true;
            if (projectsArrived && resourceArrived) {
                initProjectResources();
            }
        });

    $scope.$on('dataReady',
        function (event, data) {
            projectsArrived = true;
            if (projectsArrived && resourceArrived) {
                initProjectResources();
            }
        });

    //for multi bars on one line
    function createBox(sizes, class_name) {
        var box = document.createElement('div');
        box.style.cssText = [
            "height:" + sizes.height + "px",
            "line-height:" + sizes.height + "px",
            "width:" + sizes.width + "px",
            "top:" + sizes.top + 'px',
            "left:" + sizes.left + "px",
            "position:absolute"
        ].join(";");
        box.className = class_name;
        return box;
    }

    projectResourceGantt.templates.grid_row_class = projectResourceGantt.templates.task_class = function (start, end, task) {
        var css = [];
        if (projectResourceGantt.hasChild(task.id)) {
            css.push("task-parent");
        }
        if (!task.$open && projectResourceGantt.hasChild(task.id)) {
            css.push("task-collapsed");
        }
        return css.join(" ");
    };

    projectResourceGantt.addTaskLayer(function show_hidden(task) {
        if (!task.$open && projectResourceGantt.hasChild(task.id)) {
            var sub_height = projectResourceGantt.config.row_height - 5,
                el = document.createElement('div'),
                sizes = projectResourceGantt.getTaskPosition(task);

            var sub_tasks = projectResourceGantt.getChildren(task.id);

            var child_el;

            for (var i = 0; i < sub_tasks.length; i++) {
                var child = projectResourceGantt.getTask(sub_tasks[i]);
                var child_sizes = projectResourceGantt.getTaskPosition(child);

                child_el = createBox({
                    height: sub_height,
                    top: sizes.top,
                    left: child_sizes.left,
                    width: child_sizes.width
                }, "child_preview gantt_task_line");
                // child_el.innerHTML = child.text;
                el.appendChild(child_el);
            }
            return el;
        }
        return false;
    });

    getUniqueId2 = function (id) {
        for (var i = 0; i < projectResourceDictionary.length; i++) {
            if (id == projectResourceDictionary[i].ganttId) {
                id++;
                i = 0;
            }
        }
        return id;
    }

    getResource2 = function (id) {
        for (var index = 0; index < shared.resources.length; index++) {
            if (shared.resources[index].ID == id) {
                return shared.resources[index];
            }
        }
        return null;
    }

    $scope.$on('roles:updated', function (event, data) {
        //Create custom array for lightbox
        opts = [];
        for (var i = 0; i < data.length; i++) {
            opts.push({ key: data[i].Id, label: data[i].Name });
        }

        projectResourceGantt.form_blocks["my_editor"] = {
            render: function (sns) {
                var str = "<div class='dhx_cal_ltext' style='height:60px;'><select id=\"SelectRoleProjectResource\" size=\"4\" multiple>";
                for (index in opts)
                    str += "<option value=" + opts[index].key + ">" + opts[index].label + "</option>";
                str += "</select></div>";
                return str;
            },
            set_value: function (node, value, task, section) {
                var options = document.getElementById("SelectRoleProjectResource").options;
                if (task.roles)
                    for (index in options) {
                        if (task.roles.indexOf(parseInt(options[index].value)) > -1)
                            options[index].selected = true;
                        else
                            options[index].selected = false;
                    }
                else
                    for (index in options)
                        options[index].selected = false;
            },
            get_value: function (node, task, section) {
                var selected = [];
                var options = document.getElementById("SelectRoleProjectResource").options;

                for (index in options) {
                    if (options[index].selected == true) {
                        selected.push(parseInt(options[index].value));
                    }
                };
                return selected;
            }
        };
    });

    initProjectResources = function () {
        data = [];
        usedIds = [];
        projectResourceDictionary = [];

        for (var i = 0; i < shared.projects.length; i++) {
            var project = shared.projects[i];
            var projectItem = {
                text: project.Name,
                start_date: project.StartDate,
                id: getUniqueId2(project.ID),
                duration: 0,
                type: "project",
                projectStatus: project.Status
            }

            projectResourceDictionary.push({ dbId: project.ID, ganttId: projectItem.id });
            usedIds.push(projectItem.id);
            data.push(projectItem);

            for (var j = 0; j < project.children.length; j++) {
                var task = project.children[j]
                for (var k = 0; k < task.UserToTasks.length; k++) {
                    var relationship = task.UserToTasks[k]

                    var resource = getResource2(relationship.UserId)
                    var item = {
                        text: resource.Name,
                        first: resource.Name.split(", ")[1],
                        last: resource.Name.split(", ")[0],
                        start_date: task.StartDate,
                        end_date: task.EndDate,
                        //duration: 0,
                        id: getUniqueId2(resource.ID),
                        parent: projectItem.id,
                        roles: [],
                        type: "task"
                    };
                    for (index in resource.Roles) {
                        item.roles.push(resource.Roles[index].Id);
                    }

                    projectResourceDictionary.push({ dbId: resource.ID, ganttId: item.id })
                    data.push(item);
                }
            };
        }
        formattedData = JSON.stringify({ data: data });
        projectResourceGantt.clearAll();
        projectResourceGantt.parse(formattedData);

        shared.masterKeys.push({ view: "projectResourceGantt", keys: usedIds });
    }

    /*--------------Configure Preview---------------*/
    $scope.ChangedItems = [];
    $scope.DeletedItems = [];
    $scope.AddedItems = [];

    $scope.isPreviewing = shared.isPreviewing;
    $scope.$on('preview:updated', function (event, data) { $scope.togglePreview(data[0], data[1]); });

    $scope.togglePreview = function (isPreviewing, confirm) {
        $scope.isPreviewing = isPreviewing;
        if (!isPreviewing) {//If being set from true to false
            if (confirm) {
                for (var items in $scope.AddedItems) {
                    item = $scope.AddedItems[items];
                    shared.addProject(item);
                }
                for (var items in $scope.ChangedItems) {
                    item = $scope.ChangedItems[items];
                    if (item.type == "task") {
                        var resource = {};
                        for (var i = 0; i < shared.resources.length; i++) {
                            if (getDatabaseId(id) == shared.resources[i].ID) {
                                resource = shared.resources[i];
                                break;
                            }
                        }
                        resource.Name = item.last + ", " + item.first;
                        resource.Id = resource.ID;
                        resource.Roles = [];
                        for (var i = 0; i < item.roles.length; i++)
                            for (var j = 0; j < shared.roles.length; j++) {
                                if (shared.roles[j].Id == item.roles[i])
                                    resource.Roles.push(shared.roles[j]);
                            }
                        shared.updateResource(resource);
                    }
                    else {
                        shared.updateProject(item);
                    }
                }
                for (var items in $scope.DeletedItems) {
                    item = $scope.DeletedItems[items];
                    shared.deleteTask(item);
                }
            }
            else {
                shared.getInitialData();
            }
            $scope.ChangedItems = [];
            $scope.AddedItems = [];
            $scope.DeletedItems = [];
        }
    };
    /*--------------Configure Preview---------------*/

    /*-----------Attatch Functions to Gantt Events----------*/

    //Only projects can be added in this view, not resources
    //because resources in this view are dependent on a task relationship to the project
    //so a task must be added first and then the resource will be added on its own
    projectResourceGantt.attachEvent("onBeforeTaskAdd", function (id, item) {
        if ($scope.isPreviewing) { $scope.AddedItems.push(item); return true;}
        else {
            shared.addProject(item);
            return true;
        }
    });

    //tasks are updated using a resource lightbox
    //projects are updated using a project lightbox
    projectResourceGantt.attachEvent("onAfterTaskUpdate", function (id, item) {
        if ($scope.isPreviewing) { $scope.ChangedItems.push(item); }
        else {
            if (item.type == "task") {
                var resource = {};
                for (var i = 0; i < shared.resources.length; i++) {
                    if (getDatabaseId(id) == shared.resources[i].ID) {
                        resource = shared.resources[i];
                        break;
                    }
                }
                resource.Name = item.last + ", " + item.first;
                resource.Id = resource.ID;
                resource.Roles = [];
                for (var i = 0; i < item.roles.length; i++)
                    for (var j = 0; j < shared.roles.length; j++) {
                        if (shared.roles[j].Id == item.roles[i])
                            resource.Roles.push(shared.roles[j]);
                    }
                shared.updateResource(resource);
            }
            else {
                shared.updateProject(item);
            }
        }
    });

    //Resources cannot be deleted, only projects
    projectResourceGantt.attachEvent("onBeforeTaskDelete", function (id, item) {
        if (item.type == "task")
            return false;
        else if ($scope.isPreviewing) { $scope.DeletedItems.push(item); }
        else {
            shared.deleteTask(item);
        }
    });

    //When adding new project lightbox type defaults to projects
    projectResourceGantt.attachEvent('onTaskCreated', function (task) {
        if (!task.parent)
            task.type = "project";
        else
            return false;
        return true;
    });

    /*-----------Attatch Functions to Gantt Events----------*/
}])