﻿//custom.js --custom startup scrips and some hacky jQuery

function adminMode() {

}
/*
schedulerTab() -- loads a dhtmlxscheduler tab

*/
/*
remove / disable ui elements for admin funcrions  
*/
function readOnly() {
    $("#ctrlpanel").hide();
    //css hacks to remove / disable edit icons
    $("#previewCtrl").hide();
    $(".gantt_add").html('<i class="fa fa-arrows"></i>');
    //disable  lightboxes
    roleGantt.attachEvent("onBeforeLightbox", function (id) {console.log('blocked lightbox'); return false;});
    projectGantt.attachEvent("onBeforeLightbox", function (id) {console.log('blocked lightbox'); return false;});
    resourceGantt.attachEvent("onBeforeLightbox", function (id) {console.log('blocked lightbox'); return false;});
    // set preview mode to true disable preview toggle
    //angular.element(document.getElementById('ctrl2')).scope().togglePreview('true');
     $("body").prepend("<div class='alert alert-danger' role='alert'><i class='fa fa-exclamation'></i> WARNING: <b>You are in read only mode.</b></div>");
}
/*
startUp() -- checks for missing slash in url and intializes data
*/
function startUp(account) {
    if (account.authrole === 'Admin') {
        $(".disableonview button").prop('disabled', false);
    }
    else 
        readOnly();
        
    $('#userLoginname').html(account.username);
    //fixes Angular Factory Bug
    var currentUrl = String(window.location.href);
    if (currentUrl[currentUrl.length - 1] != '/') window.location.assign(currentUrl + '/');
    angular.element(document.getElementById('mainView')).scope().startUp();
}