﻿/* global app */
app.factory('milestoneFactory', ['$http', function ($http) {
    var urlBase = './api/milestones';
    var milestoneFactory = {};

    milestoneFactory.getMilestones = function () {
        return $http.get(urlBase);
    };

    milestoneFactory.getMilestone = function (id) {
        return $http.get(urlBase + '/' + id);
    };

    milestoneFactory.insertMilestone = function (milestone) {
        return $http.post(urlBase, milestone);
    };

    milestoneFactory.updateMilestone = function (milestone) {
        return $http.put(urlBase + '/' + milestone.ID, milestone)
    };

    milestoneFactory.deleteMilestone = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

    return milestoneFactory; 


}]);
