﻿//Initialize each Gantt Chart Tab
projectGantt = Gantt.getGanttInstance();
resourceGantt = Gantt.getGanttInstance();
roleGantt = Gantt.getGanttInstance();
projectResourceGantt = Gantt.getGanttInstance();

//Get Gantt to accept dates as MM/dd/yyyy instead of dd/MM/yyyy
projectGantt.config.xml_date = "%m-%d-%Y";
resourceGantt.config.xml_date = "%m-%d-%Y";
roleGantt.config.xml_date = "%m-%d-%Y";
projectResourceGantt.config.xml_date = "%m-%d-%Y";

//begin gantt config
projectGantt.config.scale_unit = "week";
projectResourceGantt.config.scale_unit = "week";
resourceGantt.config.scale_unit = "week";
roleGantt.config.scale_unit = "week";

//columns
projectGantt.config.columns = [
    { name: "text", label: "Name", width: "*", tree: true },
    { name: "start_date", label: "Start Time", align: "center" },
    { name: "end_date", label: "End Time", align: "center" },
    { name: "add", label: "", width: 44 }
];

resourceGantt.config.columns = [
    { name: "text", label: "Name", width: "*", tree: true },
    { name: "start_date", label: "Start Time", align: "center" },
    { name: "end_date", label: "End Time", align: "center" },
    { name: "add", label: "", width: 44 }
];
roleGantt.config.columns = [
    { name: "text", label: "Name", width: "*", tree: true },
    { name: "start_date", label: "Start Time", align: "center" },
    { name: "end_date", label: "End Time", align: "center" },
    { name: "add", label: "", width: 44 }
];
projectResourceGantt.config.columns = [
    { name: "text", label: "Name", width: "*", tree: true },
    { name: "start_date", label: "Start Time", align: "center" },
    { name: "end_date", label: "End Time", align: "center" },
    { name: "add", label: "", width: 44 }
];

//enable drag and drop
projectGantt.config.order_branch = true;
roleGantt.config.order_branch = true;
resourceGantt.config.order_branch = true;
projectResourceGantt.config.order_branch = true;

//restricts drag and drop so task can't move between projects
projectGantt.attachEvent("onBeforeTaskMove", function (id, parent, tindex) {
    var task = gantt.getTask(id);
    if (task.parent != parent)
        return false;
    return true;
});
roleGantt.attachEvent("onBeforeTaskMove", function (id, parent, tindex) {
    var task = gantt.getTask(id);
    if (task.parent != parent)
        return false;
    return true;
});
resourceGantt.attachEvent("onBeforeTaskMove", function (id, parent, tindex) {
    var task = gantt.getTask(id);
    if (task.parent != parent)
        return false;
    return true;
});
projectResourceGantt.attachEvent("onBeforeTaskMove", function (id, parent, tindex) {
    var task = gantt.getTask(id);
    if (task.parent != parent)
        return false;
    return true;
});

//dynamically set scales
setScaleDay = function () {
    projectGantt.config.scale_unit = "day";
    projectGantt.config.date_scale = "%j %M";

    resourceGantt.config.scale_unit = "day";
    resourceGantt.config.date_scale = "%j %M";

    roleGantt.config.scale_unit = "day";
    roleGantt.config.date_scale = "%j %M";

    projectResourceGantt.config.scale_unit = "day";
    projectResourceGantt.config.date_scale = "%j %M";

    projectGantt.render();
    roleGantt.render();
    resourceGantt.render();
    projectResourceGantt.render();
};
setScaleWeek = function () {
    projectGantt.config.scale_unit = "week";
    projectGantt.config.date_scale = "%j, %M";
    
    resourceGantt.config.scale_unit = "week";
    resourceGantt.config.date_scale = "%j, %M";

    roleGantt.config.scale_unit = "week";
    roleGantt.config.date_scale = "%j, %M";

    projectResourceGantt.config.scale_unit = "week";
    projectResourceGantt.config.date_scale = "%j, %M";

    autofocus = "true";

    projectGantt.render();
    roleGantt.render();
    resourceGantt.render();
    projectResourceGantt.render();
};
setScaleMonth = function () {
    projectGantt.config.scale_unit = "month";
    roleGantt.config.scale_unit = "month";
    resourceGantt.config.scale_unit = "month";
    projectResourceGantt.config.scale_unit = "month";

    projectGantt.config.date_scale = "%F, %Y";
    roleGantt.config.date_scale = "%F, %Y";
    resourceGantt.config.date_scale = "%F, %Y";
    projectResourceGantt.config.date_scale = "%F, %Y";

    projectGantt.render();
    roleGantt.render();
    resourceGantt.render();
    projectResourceGantt.render();
};
setScaleYear = function () {
    projectGantt.config.scale_unit = "year";
    roleGantt.config.scale_unit = "year";
    resourceGantt.config.scale_unit = "year";
    projectResourceGantt.config.scale_unit = "year";

    projectGantt.config.date_scale = "%F, %Y";
    roleGantt.config.date_scale = "%F, %Y";
    resourceGantt.config.date_scale = "%F, %Y";
    projectResourceGantt.config.date_scale = "%F, %Y";

    projectGantt.render();
    roleGantt.render();
    resourceGantt.render();
    projectResourceGantt.render();
};

var date_to_str = projectGantt.date.date_to_str(projectGantt.config.task_date);

//Add text to milestones
projectGantt.templates.rightside_text = function (start, end, task) {
    if (task.type == "milestone")
        if (task.IsHard)
            if (task.IsPaid)
                return "<b>Hard Milestone<b>" + ", " + "<b>Paid Milestone<b>";
            else
                return "<b>Hard Milestone<b>" + ", " + "<b>Unpaid Milestone<b>";
        else
            if (task.IsPaid)
                return "<b>Soft Milestone<b>" + ", " + "<b>Paid Milestone<b>";
            else
                return "<b>Soft Milestone<b>" + ", " + "<b>Unpaid Milestone<b>";
    return "";
};

/*---------------projectGantt Lightbox--------------------*/

//Use template to customize label for task lightbox
projectGantt.locale.labels.section_template = "Progress";
projectGantt.locale.labels.section_template2 = "User";
projectGantt.locale.labels.section_template3 = "Comments";

//Customize Task Lightbox 
//includes Percentage and Resources
projectGantt.config.lightbox.sections = [
    { name: "description", height: 38, map_to: "text", type: "textarea", focus: true },
    //{ name: "template", height: 25, map_to: "progress", type: "textarea" },
    { name: "template2", height: 25, map_to: "users", type: "my_editor" },
    { name: "template3", height: 38, map_to: "comment", type: "textarea" },
    { name: "type", type: "typeselect", map_to: "type", id: "typeselect" },
    { name: "time", height: 72, type: "duration", map_to: "auto" }
];

projectGantt.config.lightbox.project_sections = [{ name: "description", height: 70, map_to: "text", type: "textarea", focus: !0 },
                                            { name: "type", type: "typeselect", map_to: "type" },
                                            {
                                                name: "template6", type: "select", map_to: "projectStatus", options: [{ key: "planned", label: "Planned" },
                                                                                             { key: "active", label: "Active" },
                                                                                             { key: "closed", label: "Closed" }]
                                            },
                                            { name: "time", type: "duration", readonly: !0, map_to: "auto" }
];

//Customize Milestone Lightbox 
//includes iIsHard and IsPaid
projectGantt.config.lightbox.milestone_sections = [
{ name: "description", height: 25, map_to: "text", type: "textarea", focus: !0 },
{ name: "template4", height: 25, map_to: "IsHard", type: "select", options: [{ key: false, label: "Soft Deadline" }, { key: true, label: "Hard Deadline" }] },
{ name: "template5", height: 25, map_to: "IsPaid", type: "select", options: [{ key: false, label: "Not Payable Milestone" }, { key: true, label: "Payable Milestone" }] },
{ name: "type", type: "typeselect", map_to: "type" },
{ name: "time", type: "duration", single_date: !0, map_to: "auto" }
];

//Use template to customize label for milestone lightbox
projectGantt.locale.labels.section_template4 = "Is Hard";
projectGantt.locale.labels.section_template5 = "Is Paid";
projectGantt.locale.labels.section_template6 = "Project Status";

/*---------------projectGantt Lightbox--------------------*/

/*---------------resourceGantt Lightbox--------------------*/

//Use template to customize label for task lightbox
resourceGantt.locale.labels.section_template = "Percentage";
resourceGantt.locale.labels.section_template3 = "Comments";
resourceGantt.locale.labels.section_template4 = "First Name";
resourceGantt.locale.labels.section_template5 = "Last Name";
resourceGantt.locale.labels.section_template6 = "Roles";

//Customize Task Lightbox 
//includes Percentage and Resources
resourceGantt.config.lightbox.sections = [
    { name: "description", height: 38, map_to: "text", type: "textarea", focus: true },
    { name: "template", height: 25, map_to: "percent", type: "textarea" },
    { name: "template3", height: 38, map_to: "comment", type: "textarea" },
    { name: "type", type: "typeselect", map_to: "type", id: "typeselect" },
    { name: "time", height: 72, type: "duration", map_to: "auto" }
];

resourceGantt.config.lightbox.project_sections = [
    { name: "template4", height: 25, map_to: "first", type: "textarea" },
    { name: "template5", height: 25, map_to: "last", type: "textarea" },
    { name: "template6", height: 25, map_to: "roles", type: "my_editor" },
    { name: "type", type: "typeselect", map_to: "type", id: "typeselect" },
];
/*---------------resourceGantt Lightbox--------------------*/

/*---------------roleGantt Lightbox--------------------*/

roleGantt.config.lightbox.sections = [
    { name: "description", height: 25, map_to: "text", type: "textarea", focus: true }
];
roleGantt.config.lightbox.project_sections = roleGantt.config.lightbox.sections;


//Use template to customize label for task lightbox
projectResourceGantt.locale.labels.section_template = "Percentage";
projectResourceGantt.locale.labels.section_template3 = "Comments";
projectResourceGantt.locale.labels.section_template4 = "First Name";
projectResourceGantt.locale.labels.section_template5 = "Last Name";
projectResourceGantt.locale.labels.section_template6 = "Roles";
projectResourceGantt.locale.labels.section_template7 = "Status";

/*---------------roleGantt Lightbox--------------------*/

/*---------------projectResourceGantt Lightbox--------------------*/

//Customize Project Lightbox
projectResourceGantt.config.lightbox.project_sections = [{ name: "description", height: 70, map_to: "text", type: "textarea", focus: !0 },
                                            { name: "type", type: "typeselect", map_to: "type" },
                                            {
                                                name: "template7", type: "select", map_to: "projectStatus", options: [{ key: "planned", label: "Planned" },
                                                                                                                        { key: "active", label: "Active" },
                                                                                                                        { key: "closed", label: "Closed" }]
                                            }
];

//Customize Task Lightbox for Resources
projectResourceGantt.config.lightbox.sections = [
    { name: "template4", height: 25, map_to: "first", type: "textarea" },
    { name: "template5", height: 25, map_to: "last", type: "textarea" },
    { name: "template6", height: 25, map_to: "roles", type: "my_editor" },
    { name: "type", type: "typeselect", map_to: "type", id: "typeselect" },
];

/*---------------projectResourceGantt Lightbox--------------------*/