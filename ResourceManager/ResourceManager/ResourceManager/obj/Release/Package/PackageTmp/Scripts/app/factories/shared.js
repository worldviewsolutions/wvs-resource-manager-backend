﻿app.factory('shared', ['$rootScope', 'projectFactory', 'taskFactory', 'resourceFactory', 'milestoneFactory', 'roleFactory',
    function ($rootScope, projectFactory, taskFactory, resourceFactory, milestoneFactory, roleFactory) {
        var shared = {};
        
        shared.expandCollapseStatus = "Expand All"
        shared.masterKeys = []; //dictionary of root components from each view
        shared.showArchived = false;
        shared.selectedItems = []; //filter output
        shared.filterItems = [];  //filter options
        shared.setPreview = function (newValue) {
            if (newValue == false) {
                var saveChanges = confirm("Do you want to save your changes?");
            } $rootScope.$broadcast('preview:updated', [newValue, saveChanges]);
        };
        shared.setBeginView = function (newValue) { $rootScope.$broadcast('BeginView:updated', newValue); };
        shared.setEndView = function (newValue) { $rootScope.$broadcast('EndView:updated', newValue); };

        shared.idDictionary = [];
        shared.activeTab = "projectGantt";

        shared.waitingToUpdate = null;
        shared.waitingToAdd = null;

        shared.updateExpandCollapse = function (status) {
            shared.expandCollapseStatus = status;
            $rootScope.$broadcast('ExpandCollapse: updated');
        }

        shared.getInitialData = function () {
            console.log("ddddddddddddddd", shared.showArchived)
            taskFactory.getTasks().success(function (tasks) {
                projectFactory.getProjects()
                    .success(function (projects) {
                        //dont show archived projects unless button is clicked for it
                        if (shared.showArchived == false) {
                            for (var i = 0; i < projects.length; i++) {
                                if (projects[i].Status == "closed") {
                                    projects.splice(i, 1);
                                }
                            };
                            for (var i = 0; i < tasks.length; i++) {
                                if (tasks[i].Project.Status == "closed") {
                                    tasks.splice(i, 1);   ///tasks with archived project are removed
                                }
                            };
                        }

                        shared.tasks = tasks;
                        shared.projects = projects;
                        milestoneFactory.getMilestones()
                        .success(function (milestones) {
                            shared.milestones = milestones;
                            $rootScope.$broadcast('dataReady', [tasks, projects, milestones]);
                            $rootScope.$broadcast('events:updated', []);
                        })
                        .error(function(error){
                            console.log("Unable to load data: " + error.message);
                        })
                        for (var index = 0; index < projects.length; index++) {
                            //create filter objects
                            var filter = {
                            'name': projects[index].Name,
                            'icon': '<i class="fa fa-ban"></i>',
                            'ticked': false,
                            'marker': projects[index].Name
                             };
                             shared.filterItems.push(filter);
        }
                    })
                    .error(function (error) {
                        console.log('Unable to load data: ' + error.message);
                    });
            })
                .error(function (error) {
                    console.log('Unable to load data: ' + error.message);
                });
        };

        shared.resources = [];
        shared.getResources = function () {
            resourceFactory.getResources()
                .success(function (resources) {
                    shared.resources = resources;
                    $rootScope.$broadcast('resources:updated', resources); 
                   })
                .error(function (error) {
                    console.log( 'Unable to load resource data: ' + error.message);
                });
        };

        shared.projects = [];
        shared.getProjects = function () {
            projectFactory.getProjects()
                .success(function (projects) {
                    shared.projects = projects;
                })
                .error(function (error) {
                    console.log('Unable to load project data: ' + error.message);
                })
        };

        shared.tasks = [];
        shared.getTasks = function () {
            taskFactory.getTasks()
                .success(function (tasks) {
                    shared.tasks = tasks;  
                })
                .error(function (error) {
                    console.log( 'Unable to load task data: ' + error.message);
                })
            };

        shared.milestones = [];
        shared.getMilestones = function () {
            milestoneFactory.getMilestones()
                .success(function (milestones) {
                    shared.milestones = milestones;  
                })
                .error(function (error) {
                    console.log( 'Unable to load milestone data: ' + error.message);
                })
        };

        shared.roleFilter = [];
        shared.roles = [];
        shared.getRoles = function () {
            roleFactory.getRoles()
                .success(function (roles) {
                    shared.roles = roles;
                    $rootScope.$broadcast('roles:updated', shared.roles);
               for (var index = 0; index < roles.length; index++) {
                            //create filter objects
                            var filter = {
                            'name': roles[index].Name,
                            'users':roles[index].Users,
                            'icon': '<i class="fa fa-ban"></i>',
                            'ticked': false,
                            'marker': roles.Id
                             };
                           shared.roleFilter.push(filter);
                           }
                }).error(function (error) {
                    console.log("Unable to load roles data: " + error.message);
                });
        }

        /*---------Add, Update, and Delete Functions-----------*/

        //Add color to milestones
        setMilestoneColor = function (isHard, isPaid) {
            if (isHard)
                if (isPaid)
                    return "#FF0000";
                else
                    return "#FFFF00";
            else
                if (isPaid)
                    return "#00FF00";
                else
                    return "#0000FF";
        };

        shared.addProject = function (newproject) {
            var project = {
                "Name": newproject.text,
                "StartDate": newproject.start_date.toLocaleDateString(),
                "EndDate": newproject.end_date.toLocaleDateString(),
                "LastModified": new Date().toLocaleDateString(),
                "Status": newproject.projectStatus,
                "children": []
            };

            //Upload project to database and on success reload database
            projectFactory.insertProject(project)
                .success(function () {
                    //Have to reload tree_data or new ID is unavailble
                    shared.getInitialData();
                });
        };

        shared.addResource = function (resource) {
            resourceFactory.insertResource(resource)
                .success(function () {
                    shared.getResources();
                    shared.getRoles();
                }).error(function (error) {
                    console.log("New user not saved: " + error.message)
                    console.log(resource);
                })
        };

        shared.addTask = function (newtask) {
            var task = {
                "Name": newtask.text,
                "ProjectId": newtask.parent,
                "StartDate": newtask.start_date.toLocaleDateString(),
                "EndDate": newtask.end_date.toLocaleDateString(),
                "LastModified": new Date().toLocaleDateString(),
                "Comment": newtask.comment,
                "UserToTasks": newtask.UserToTasks
            };

            //Upload task to database and on success reload local arrays
            taskFactory.insertTask(task)
                .success(function () {
                    //Update projects when their tasks are added, project time span will be based on tasks
                    shared.updateProject(projectGantt.getTask(newtask.parent));
                    shared.getResources();
                }).error(function (error) {
                    console.log("New task not saved: " + error.message);
                    console.log(task);
                });
        };

        shared.addMilestone = function (newmilestone) {
            var milestone = {
                IsPaid: newmilestone.IsPaid,
                StartTime: newmilestone.start_date.toLocaleDateString(),
                Date: newmilestone.start_date.toLocaleDateString(),
                IsHard: newmilestone.IsHard,
                ProjectId: newmilestone.parent,
                Title: newmilestone.text
            };

            milestoneFactory.insertMilestone(milestone)
            .success(function () {
                //Update projects when their milestones are added, project time span will be based on tasks
                shared.updateProject(projectGantt.getTask(newmilestone.parent));
            }).error(function (error) {
                console.log("New milestone not updated: " + error.message); 
                console.log(milestone);
            });
        };

        shared.addRole = function (newRole) {
            roleFactory.insertRole({ "Name": newRole.text })
            .success(function(){
                shared.getRoles();
            })
            .error(function (error) {
                console.log("New Role not updated: " + error.message);
                console.log(newRole);
            })
        };

        shared.updateProject = function (newproject) {
            var project = {
                "Name": newproject.text,
                "Id": newproject.id,
                "StartDate": newproject.start_date.toLocaleDateString(),
                "EndDate": newproject.end_date.toLocaleDateString(),
                "Status": newproject.projectStatus,
                "LastModified": new Date().toLocaleDateString(),
            };
            //Upload project to database and on success update local array
            projectFactory.updateProject(project)
                .success(function () {
                    shared.getInitialData();
                })
              .error(function (error) {
                  console.log("Unable to update project: " + error.message);
                  console.log(project);
              });
        };

        shared.updateResource = function (resource) {
            resourceFactory.updateResource(resource)
                .success(function () {
                    shared.getResources();
                    shared.getRoles();
                })
              .error(function (error) {
                  console.log('Unable to update resource: ' + error.message);
                  console.log(resource);
              });
        };

        getUser = function (id) {
            index = indexByIdArray(parseInt(id), shared.resources);
            return shared.resources[index];
        };

        //these two can probably be combined
        shared.updateTask = function (newTask) {
            var task = {
                "Name": newTask.text,
                "ID": shared.getDataBaseId(newTask.id),
                "StartDate": newTask.start_date.toLocaleDateString(),
                "EndDate": newTask.end_date.toLocaleDateString(),
                "LastModified": new Date().toLocaleDateString(),
                "Comment": newTask.comment,
                "UserToTasks": newTask.UserToTasks
            };
            
            //Upload task to the database and on success update local array with OldTask
            taskFactory.updateTask(task)
            .success(function () {
                //Update projects when their tasks are updated, project time span will be based on tasks
                    shared.updateProject(projectGantt.getTask(newTask.parent));
                //shared.getInitialData();
                shared.getResources();

            }).error(function (error) {
                console.log("Unable to update task: " + error.message);
                console.log(task);
            });
        };

        shared.updateMilestone = function (newmilestone) {
            var milestone = {
                ID: shared.getDataBaseId(newmilestone.id),
                IsPaid: newmilestone.IsPaid || false,
                IsHard: newmilestone.IsHard || false,
                StartTime: newmilestone.start_date.toLocaleDateString(),
                Date: newmilestone.start_date.toLocaleDateString(),
                ProjectId: newmilestone.parent,
                Title: newmilestone.text
            };

            milestoneFactory.updateMilestone(milestone)
                .success(function () {
                    //Update projects when their milestones are updated, project time span will be based on tasks
                    shared.updateProject(projectGantt.getTask(newmilestone.parent));
                }).error(function (error) {
                    console.log("unable to update milestone: " + error.message);
                    console.log(milestone);
                });
        };

        shared.updateRole = function (newrole) {
            roleFactory.updateRole(newrole)
            .success(function () {
                shared.getRoles();
                shared.getResources();
            })
            .error(function (error) {
                console.log("unable to update role: " + error.message);
                console.log(newrole);
            })
        };

        shared.deleteTask = function (task) {
            var id;
            if (task.id)
                id = task.id;
            else
                id = task.ID;

            if (task.type == "project") {
                projectFactory.deleteProject(id)
                .error(function () {
                    console.log("Unable to delete project: " + error.message);
                    console.log(task);
                });
            }
            else if (task.type == "task") {
                taskFactory.deleteTask(shared.getDataBaseId(id))
                    .success(function () {
                        shared.getResources();
                    })
                .error(function () {
                    console.log("Unable to delete task: " + error.message);
                    console.log(task);
                });
            }
            else if (task.type == "milestone") {
                milestoneFactory.deleteMilestone(shared.getDataBaseId(id))
                .error(function () {
                    console.log("Unable to delete milestone: " + error.message);
                    console.log(task);
                });
            }
        };

        shared.deleteRole = function (role) {
            roleFactory.deleteRole(role.Id)
            .error(function (error) {
                console.log("unable to delete role: " + error.message);
                console.log(role);
            })
        };

        /*---------Add and Update Functions-----------*/

        shared.getDataBaseId = function (ganttId) {
            for (i = 0; i < shared.idDictionary.length; i++)
                if (shared.idDictionary[i].ganttId == ganttId)
                    return shared.idDictionary[i].dataBaseId;
            return null;
        };

        shared.hideDateField = false;

        indexByIdArray = function (ID, array) {
            var i = 0,
                len = array.length;
            for (i = 0; i < len; i++)
                if (array[i].ID == ID)
                    return i;
            return null;
        };

        return shared;
    }]);