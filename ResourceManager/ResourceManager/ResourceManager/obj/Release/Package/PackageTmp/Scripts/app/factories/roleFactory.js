﻿app.factory('roleFactory', ['$http', function ($http) {
    var urlBase = './api/roles';
    var roleFactory = {};

    roleFactory.getRoles = function () {

        return $http.get(urlBase);
    };

    roleFactory.getRole = function (id) {
        return $http.get(urlBase + '/' + id);
    };

    roleFactory.insertRole = function (role) {
        console.log("Role being inserted: ", role)
        return $http.post(urlBase, role);
    };

    roleFactory.updateRole = function (role) {
        return $http.put(urlBase + '/' + role.Id, role)
    };

    roleFactory.deleteRole = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

    return roleFactory;

}]);