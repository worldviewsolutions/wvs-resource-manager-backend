app.directive('dhxScheduler', ['shared', function (shared) {
    return {
        restrict: 'A',
        scope: false,
        transclude: true,
        template:'<div class="dhx_cal_navline" ng-transclude></div><div class="dhx_cal_header"></div><div class="dhx_cal_data"></div>',
        link:function ($scope, $element, $attrs, $controller){
              
            //default state of the scheduler
            if (!$scope.scheduler)
                $scope.scheduler = {};
            $scope.scheduler.mode = $scope.scheduler.mode || "month";
            $scope.scheduler.date = $scope.scheduler.date || new Date();

            //watch data collection, reload on changes
            $scope.$watch($attrs.data, function(collection){
                scheduler.clearAll();
                scheduler.parse(collection, "json");
            }, true);

            //mode or date
            $scope.$watch(function(){
                return $scope.scheduler.mode + $scope.scheduler.date.toString();
            }, function(nv, ov) {
                var mode = scheduler.getState();
                if (nv.date != mode.date || nv.mode != mode.mode)
                    scheduler.setCurrentView($scope.scheduler.date, $scope.scheduler.mode);
            }, true);

            //size of scheduler
            $scope.$watch(function() {
                return $element[0].offsetWidth + "." + $element[0].offsetHeight;
            }, function() {
                scheduler.setCurrentView();
            });
            scheduler.locale.labels.timeline_tab ="Resource Calender";
            scheduler.locale.labels.timeline2_tab ="Project Calender";
            //styling for dhtmlx scheduler
            $element.addClass("dhx_cal_container");
            //calls scheduleResources factory and formats Timeline view

            $scope.$on('events:updated', function (event, data) {loadCalendar(); });
            $scope.$on('resources:updated', function (event, data) { loadTimeLine(); loadRoles(); });
            $scope.$on('roles:updated', function (event, data) { loadRoles(); });

            //Load resources for Resource Calendar
            loadTimeLine = function () {
                var timelineKey = [];
                var resources = shared.resources;
                for (var i = 0; i < resources.length; i++) {
                    var currentResource = resources[i];
                    var resource = {
                        "key": currentResource.ID,
                        "label": currentResource.Name + ' ' + currentResource.ID
                    };
                    timelineKey.push(resource);
                }
                scheduler.createTimelineView({
                    name: 'timeline',
                    x_unit: "day",//to appear the days of the month
                    x_date: "%j", //number of the day
                    x_step: 1, //to show the all days
                    x_size: 31, // number of days in month( max)
                    x_start: 0, // zero offset from a current calendar date
                    x_length: 31, //the number of days that goes back and forward on prev and next button
                    y_unit: timelineKey,
                    round_position: true,
                    y_property: "assigned_to",
                    render: "bar",
                    event_dy: "full"
                });
            };

            //Load projects for Project Calendar        
            loadCalendar = function () {
                var resourcesforTimeline = [];
                var projects = shared.projects;
                for (var i = 0; i < projects.length; i++) {
                    var currentProj = projects[i];
                    var projectUsers = [];
                    var currentProjtasks = currentProj.children;
                    for (var j = 0; j < currentProjtasks.length; j++) {
                        var taskuserList = currentProjtasks[j].UserToTasks;
                        if (taskuserList)
                            for (var k = 0; k < taskuserList.length; k++) {
                                var currentTaskResource = taskuserList[k];
                                var resource = {
                                    "key": currentTaskResource.UserId,
                                    "open": true,
                                    "label": currentTaskResource.LastName + ", " + currentTaskResource.FirstName
                                };
                                projectUsers.push(resource);
                            }
                    }
                    var element = {
                        "key": currentProj.ID,
                        "label": currentProj.Name,
                        "children": projectUsers
                    };
                    resourcesforTimeline.push(element);
                }
                scheduler.createTimelineView({
                    section_autoheight: true,
                    name: 'timeline2',
                    x_unit: "day",//to appear the days of the month
                    x_date: "%j", //number of the day
                    x_step: 1, //to show the all days
                    x_size: 31, // number of days in month( max)
                    x_start: 0, // zero offset from a current calendar date
                    x_length: 31, //the number of days that goes back and forward on prev and next button
                    y_unit: resourcesforTimeline,
                    round_position: true,
                    y_property: "assigned_to",
                    render: "tree",
                    event_dy: "full"
                });
            }

            //Load roles for Role Calendar
            loadRoles = function () {
                var roleKey = [];
                var roles = shared.roles;
                for (var i = 0; i < roles.length; i++) {
                    var currentRole = roles[i];
                    var roleUsers = [];
                    if (currentRole.Users)
                        for (var k = 0; k < currentRole.Users.length; k++) {
                            var currentUser = currentRole.Users[k];
                            roleUsers.push({
                                "key": currentUser.Id,
                                "label": currentUser.LastName + ", " + currentUser.FirstName
                            });
                        };
                    roleKey.push({
                        "key": currentRole.ID,
                        "label": currentRole.Name,
                        "children": roleUsers
                    });
                }
                scheduler.createTimelineView({
                    name: 'timeline3',
                    x_unit: "day",//to appear the days of the month
                    x_date: "%j", //number of the day
                    x_step: 1, //to show the all days
                    x_size: 31, // number of days in month( max)
                    x_start: 0, // zero offset from a current calendar date
                    x_length: 31, //the number of days that goes back and forward on prev and next button
                    y_unit: roleKey,
                    round_position: true,
                    y_property: "assigned_to",
                    render: "tree",
                    event_dy: "full"
                });
            };

            //allow for tasks to be assigned to more than one resource
            scheduler.config.multisection = true;
            //init scheduler
            scheduler.init($element[0], $scope.scheduler.date, $scope.scheduler.mode);
        }
    };
}]);

app.directive('dhxTemplate', ['$filter', function($filter){
  scheduler.aFilter = $filter;

  return {
    restrict: 'AE',
    terminal:true,
    
    link:function($scope, $element, $attrs, $controller){
      $element[0].style.display = 'none';

      var template = $element[0].innerHTML;
      template = template.replace(/[\r\n]/g,"").replace(/"/g, "\\\"").replace(/\{\{event\.([^\}]+)\}\}/g, function(match, prop){
        if (prop.indexOf("|") != -1){
          var parts = prop.split("|");
          return "\"+scheduler.aFilter('"+(parts[1]).trim()+"')(event."+(parts[0]).trim()+")+\"";
        }
        return '"+event.'+prop+'+"';
      });
      var templateFunc = Function('sd','ed','event', 'return "'+template+'"');
      scheduler.templates[$attrs.dhxTemplate] = templateFunc;
    }
  };
}]);