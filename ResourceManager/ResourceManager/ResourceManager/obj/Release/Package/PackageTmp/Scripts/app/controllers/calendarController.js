/* global scheduler */
app.controller('MainSchedulerCtrl', ['$scope',  'shared', function ($scope, shared) {
    //when in preview mode, users should not be able to drag calendar elements
    scheduler.attachEvent("onBeforeDrag", function (id, mode, e){
         if( !shared.isPreviewing) { 
                   return true;
         } else { 
        alert('Please login or disable preview mode to change elements.');
        return false;
         }
    });

    //hide all nonassigned things in Project calendar so they don't float around weirdly
    scheduler.filter_timeline2 = function(id, event){
        if ( event.kind == 'project' || event.kind == 'milestone' )
            return false; // event will be filtered (not rendered)
            //or
            return true; // event will be rendered
    }

    //lightbox calls should go to dhtmlx gantt
    scheduler.attachEvent("onBeforeLightbox", function (id){
        if (shared.getDataBaseId(id) == null) {
            for (index in shared.projects)
                if (shared.projects[index].ID == id) {
                    gantt.showLightbox(id);
                    return false;
                }
            gantt.createTask({
                id: id,
                text: "new event",
                type: "project",
                start_date: scheduler.getEvent(id).start_date,
                end_date: scheduler.getEvent(id).end_date
            }, "");
            return false; 
        }
        else {
            gantt.showLightbox(id);
            return false;
        }
    });

    scheduler.config.container_autoresize = false

    var dragged_event;
    scheduler.attachEvent("onBeforeDrag", function (id, mode, e) {
        dragged_event = scheduler.getEvent(id); //use it to get the object of the dragged event
        return true;
    });
    scheduler.attachEvent("onDragEnd", function () {
     var event_obj = dragged_event;
     if (event_obj && event_obj.kind == "task") {
         for (var i = 0; i < shared.tasks.length; i++) {
             if (shared.tasks[i].ID == event_obj.id){
                 shared.tasks[i].StartDate = event_obj.start_date.toLocaleDateString();
                 shared.tasks[i].EndDate = event_obj.end_date.toLocaleDateString();
                
                 shared.updateTaskOnDrag(shared.tasks[i]);
             }
         }
         dragged_event = null;
        // shared.updateTask(event_obj);
     }
 });

    $scope.events = []; //list of calendar elements

    //getProjColors() -- generates a color for each project so we can color tasks/milestones by their project in task views
    function getProjColors(Projects) {
        var colorProjectRelationships = {};
        for (var k = 0; k < Projects.length; k++) {
            colorProjectRelationships[Projects[k].ID] = '#' + (5 * Math.floor(Math.random() * 4)).toString(16) +
          (5 * Math.floor(Math.random() * 4)).toString(16) +
          (5 * Math.floor(Math.random() * 4)).toString(16);
        }
        return colorProjectRelationships;
    }

    $scope.debugtimeline = function () { console.log($scope.events); };
    $scope.scheduler = { date: new Date() }; //get current date
    $scope.$on('events:updated', function (event, data) { $scope.loadData(); });
    $scope.loadData = function () {
        $scope.events = []; //reset data array

        // We always want to get Projects so we can relate tasks and milestones to each other via Project
        var taskColors = [];

        var projects = shared.projects;
        taskColors = getProjColors(projects);

        for (var i = 0; i < projects.length; i++) {
            var currentProject = projects[i];

            var project = {
                "kind": 'project',
                "id": currentProject.ID,
                "text": currentProject.Name,
                "start_date": new Date(currentProject.StartDate),
                "end_date": new Date(currentProject.EndDate),
                "assigned_to": 1,
                "color": "green"
            };

            if (currentProject.Status == "closed")
                project.color = "#FBF3B5";

                $scope.events.push(project);
        }

        var tasks = shared.tasks;
        for (var i = 0; i < tasks.length; i++) {
            var currentTask = tasks[i];
            var task = {
                "kind": 'task',
                "id": currentTask.ID,
                "text": currentTask.Name,
                "start_date": new Date(currentTask.StartDate),
                "end_date": new Date(currentTask.EndDate)
                //"color": taskColors[currentTask.Project.ID],
            };

            if (currentTask.Project.Status == "closed")
                task.color = "#FBF3B5";
            for (index in currentTask.UserToTasks) {
                if (index == 0)
                    task["assigned_to"] = currentTask.UserToTasks[index].UserId.toString();
                else
                    task["assigned_to"] = task["assigned_to"] + "," + currentTask.UserToTasks[index].UserId.toString();
            }

            //console.log(currentTask.Users);
            $scope.events.push(task);
        }

        var milestones = shared.milestones;
        for (var i = 0; i < milestones.length; i++) {
            var currentMilestone = milestones[i];
            var milestone = {
                "kind": 'milestone',
                "id": currentMilestone.ID,
                "text": currentMilestone.Title,
                "start_date": new Date(currentMilestone.Date),
                "end_date": new Date(currentMilestone.Date),
                "assigned_to": 1,
                //"color": taskColors[currentMilestone.ProjectID]
            };
            $scope.events.push(milestone);
        }
    };
  }]);