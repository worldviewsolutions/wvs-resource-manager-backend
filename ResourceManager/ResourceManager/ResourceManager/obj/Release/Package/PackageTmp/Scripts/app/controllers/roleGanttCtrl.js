﻿app.controller('roleGanttCtrl', ['$scope', 'shared', function ($scope, shared) {
    var data = [];
    var formattedData = [];
    var roleDictionary = [];
    var usedIds = [];

    roleGantt.init("role_gantt");

    var taskArrived = false;
    var roleArrived = false;

    $scope.$on('roles:updated',
        function (event, data) {
            roleArrived = true;
            if(roleArrived && taskArrived){
                init();
            }
        });

    $scope.$on('events:updated',
        function (event, data) {
            taskArrived = true;
            if (taskArrived && roleArrived) {
                init();
            }
        });

    //for multi bars on one line
    function createBox(sizes, class_name) {
        var box = document.createElement('div');
        box.style.cssText = [
            "height:" + sizes.height + "px",
            "line-height:" + sizes.height + "px",
            "width:" + sizes.width + "px",
            "top:" + sizes.top + 'px',
            "left:" + sizes.left + "px",
            "position:absolute"
        ].join(";");
        box.className = class_name;
        return box;
    }

    roleGantt.templates.grid_row_class = roleGantt.templates.task_class = function (start, end, task) {
        var css = [];
        if (roleGantt.hasChild(task.id)) {
            css.push("task-parent");
        }
        if (!task.$open && roleGantt.hasChild(task.id)) {
            css.push("task-collapsed");
        }

        return css.join(" ");
    };

    roleGantt.addTaskLayer(function show_hidden(task) {
        if (!task.$open && roleGantt.hasChild(task.id)) {
            var sub_height = roleGantt.config.row_height - 5,
                el = document.createElement('div'),
                sizes = roleGantt.getTaskPosition(task);

            var sub_tasks = roleGantt.getChildren(task.id);

            var child_el;

            for (var i = 0; i < sub_tasks.length; i++) {
                var child = roleGantt.getTask(sub_tasks[i]);
                var child_sizes = roleGantt.getTaskPosition(child);

                child_el = createBox({
                    height: sub_height,
                    top: sizes.top,
                    left: child_sizes.left,
                    width: child_sizes.width
                }, "child_preview gantt_task_line");
                // child_el.innerHTML = child.text;
                el.appendChild(child_el);
            }
            return el;
        }
        return false;
    });

    getUniqueResourceId = function (id) {

        for (var i = 0; i < roleDictionary.length; i++) {
            if (id == roleDictionary[i].ganttId) {
                id++;
                i = 0;
            }
        }
        return id;
    }

    getUser = function (id) {
        for (var i = 0; i < shared.tasks.length; i++) {
            if (shared.tasks[i].Id == id) {
                return shared.tasks[i];
            }
        }
        return null;
    }

    init = function () {
        data = [];
        roleDictionary = [];
        usedIds = [];
        for (var i = 0; i < shared.roles.length; i++) {
            var role = shared.roles[i];

            var roleItem = {
                text: role.Name,
                start_date: "4-7-2015",
                id: getUniqueResourceId(role.Id),
                type: "project"
            }
            roleDictionary.push({ dbId: role.Id, ganttId: roleItem.id });
            data.push(roleItem);
            usedIds.push(roleItem.id);

            for (var j = 0; j < role.Users.length; j++) {
                var resource = role.Users[j]

                if (resource != null) {
                    var item = {
                        text: resource.LastName + ", " + resource.FirstName,
                        start_date: "1-1-2015",
                        end_date: "2-1-2015",
                        duration: 0,
                        id: getUniqueResourceId(resource.Id),
                        parent: roleItem.id,
                        type: "project"
                    }
                    roleDictionary.push({ dbId: resource.Id, ganttId: item.id })
                    data.push(item);

                    for (var k = 0; k < resource.UserToTasks.length; k++) {
                        var task = getTask1(resource.UserToTasks[k].TaskId)
                        if (task != null) {
                            var taskItem = {
                                text: task.Name,
                                start_date: task.StartDate,
                                end_date: task.EndDate,
                                id: getUniqueResourceId(task.Id),
                                parent: item.id,
                                percent: resource.UserToTasks[k].Percentage,
                                type: gantt.config.types.task
                            }
                            // console.log("projectResourceGantt: ", data)
                            roleDictionary.push({ dbId: task.Id, ganttId: taskItem.id })
                            data.push(taskItem);
                        }
                    }
                }
            }
        }
        formattedData = JSON.stringify({ data: data });
        roleGantt.parse(formattedData);

        shared.masterKeys.push({view: "roleGantt", keys: usedIds})
    }

    function getDatabaseId(id) {
        for (var i = 0; i < roleDictionary.length; i++)
            if (roleDictionary[i].ganttId == id)
                return roleDictionary[i].dbId;
    };

    /*--------------Configure Preview---------------*/
    $scope.ChangedItems = [];
    $scope.DeletedItems = [];
    $scope.AddedItems = [];

    $scope.isPreviewing = shared.isPreviewing;
    $scope.$on('preview:updated', function (event, data) { $scope.togglePreview(data[0], data[1]); });

    $scope.togglePreview = function (isPreviewing, confirm) {
        $scope.isPreviewing = isPreviewing;
        if (!isPreviewing) {//If being set from true to false
            if (confirm) {
                for (var items in $scope.AddedItems) {
                    item = $scope.AddedItems[items];
                    shared.addRole(item);
                }
                for (var items in $scope.ChangedItems) {
                    item = $scope.ChangedItems[items];
                    shared.updateRole({ "Id": getDatabaseId(item.id), "Name": item.text });
                }
                for (var items in $scope.DeletedItems) {
                    item = $scope.DeletedItems[items];
                    if (item.type == "project" && item.parent == 0)
                        shared.deleteRole(item);
                    else
                        shared.deleteTask(item);
                }
            }
            else {
                shared.getInitialData();
            }
            $scope.ChangedItems = [];
            $scope.AddedItems = [];
            $scope.DeletedItems = [];
        }
    };
    /*--------------Configure Preview---------------*/

    /*-----------Attatch Functions to Gantt Events----------*/

    //Lightbox can not be called for resources or tasks, only roles
    roleGantt.attachEvent("onBeforeLightbox", function (id) {
        if (roleGantt.getTask(id).parent != 0) {
            return false;
        }
        else
            return true;
    });
    //Only new roles will be added to the database
    roleGantt.attachEvent("onBeforeTaskAdd", function (id, item) {
        if (item.parent != 0) {
            return false;
        }
        else if ($scope.isPreviewing) { $scope.AddedItems.push(item); return true;}
        else
            shared.addRole(item);
        return true;
    });
    //Updates to roles are passed to the update Role function
    roleGantt.attachEvent("onAfterTaskUpdate", function (id, item) {
        if ($scope.isPreviewing) { $scope.ChangedItems.push(item); }
        else
        shared.updateRole({"Id": getDatabaseId(id),"Name":item.text});
    });
    //Only roles and tasks can be deleted not resources
    roleGantt.attachEvent("onAfterTaskDelete", function (id, item) {
        if (item.type == "project" && item.parent != 0)
            return false;
        else if ($scope.isPreviewing) { $scope.DeletedItems.push(item); }
        else if (item.type == "project" && item.parent == 0)
            shared.deleteRole(item);
        else
            shared.deleteTask(item);
    });

    /*-----------Attatch Functions to Gantt Events----------*/
}])