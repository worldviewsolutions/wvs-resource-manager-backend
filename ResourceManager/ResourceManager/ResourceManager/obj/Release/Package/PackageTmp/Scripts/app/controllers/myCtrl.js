app.controller('myCtrl', ['$rootScope','$scope', 'shared', 
    function ($rootScope, $scope, shared) {

        $scope.$on('resources:updated', function (event, data) { $scope.resource_tree = data; });
        $scope.$on('roles:updated', function (event, data) { $scope.roles = data; });
        $scope.$on('dataReady', function (event, args) { 
            
            $scope.tasks = args[0]; 
            $scope.projects = args[1]; 
            $scope.milestones = args[2]; 
            //hide loading screen, bring view live
            $("button").prop('disabled', false);
            $("#loadingScreen").hide();
            $("#mainContent").show();
            $('#loginModal').modal('hide');
        });

        $scope.selectedResource = "";
        $scope.selectedItems = [];
        $scope.filterItems = shared.filterItems;
        $scope.filterItemsRoles = shared.roleFilter;
        $scope.filterItemsRolesOutput = [];
        $scope.selectedUser = "";
        
        //filterGantt -- removes any gantt project with the same name as a selected filter element
        $scope.filterGantt = function() {
            var currentTasks = projectGantt.getTaskByTime(); 
            //display only selected options from selectedItems
            projectGantt.attachEvent("onBeforeTaskDisplay", function(id, task) {
                for (var index = 0; index < $scope.selectedItems.length; index++) {
                    var filterElement = $scope.selectedItems[index];
                    if (task.text === filterElement.name)
                        return false;

                }
                return true;
            }); 
            projectGantt.refreshData();
        };
        
        //filterResource -- removes any gantt project with the same name as a selected filter element
        $scope.filterResource = function() {
            var currentTasks = resourceGantt.getTaskByTime(); 
            //display only selected options from selectedItems
            resourceGantt.attachEvent("onBeforeTaskDisplay", function(id, task) {
                for (var index = 0; index < $scope.filterItemsRolesOutput.length; index++) {
                    var filterElement = $scope.filterItemsRolesOutput[index];
                    var array = filterElement.users;
                    var toFilter = false;
                    for (var index = 0; index < array.length; index++) {
                        var fullname = array[index].LastName+', '+array[index].FirstName;
                        if (task.text === fullname)
                            return false;
                    }
                }
                return true;
            }); 
            resourceGantt.refreshData();
        };

        //keep track of active tab
        $scope.setActiveTab = function (tab) {
            shared.activeTab = tab;

            projectGantt.render();
            roleGantt.render();
            resourceGantt.render();
            projectResourceGantt.render();
        }

        /*------------------Expand/Collapse--------------------------------------*/
        $scope.expandCollapseAll = function (action) {
            var localKeys;

            for (i in shared.masterKeys) {                
                 if (shared.activeTab == shared.masterKeys[i].view) {
                     localKeys = shared.masterKeys[i].keys;
                 }
            }
            for (i in localKeys) {               
                 switch (shared.activeTab) {
                     case "projectGantt":
                         if (action == "expand") {
                             projectGantt.open(localKeys[i].toString())
                         } else if(action == "collapse") {
                             projectGantt.close(localKeys[i].toString())
                         }
                         
                     case "roleGantt":
                         if (action == "expand") {
                             roleGantt.open(localKeys[i].toString())
                         } else if (action == "collapse") {
                             roleGantt.close(localKeys[i].toString())
                         }
                         break;
                     case "projectResourceGantt":
                         if (action == "expand") {
                             projectResourceGantt.open(localKeys[i].toString())
                         } else if (action == "collapse") {
                             projectResourceGantt.close(localKeys[i].toString())
                         }
                         break;
                     case "resourceGantt":
                         if (action == "expand") {
                             resourceGantt.open(localKeys[i].toString())
                         } else if (action == "collapse") {
                             resourceGantt.close(localKeys[i].toString())
                         }
                         break;
                 }
             }
         };
       
        /*----show/hide closed projects-------*/
        $scope.toggleArchivedLabel = "Show Archived Projects";

        $scope.toggleArchivedProjects = function () {
            shared.showArchived = !shared.showArchived;
            if (shared.showArchived) {
                $scope.toggleArchivedLabel = "Hide Archived Projects";
            } else {
                $scope.toggleArchivedLabel = "Show Archived Projects";
            }
            shared.getInitialData();
        };
        /*----show/hide closed projects-------*/

        /*------------Add/Manage Resources------------*/
        $scope.selectedRoles = [];
        $scope.hideNewRoleField = false;

        $scope.manageResource = function () {
            var splitName = $scope.selectedResource.Name;
            splitName = splitName.split(", ");
            $scope.first = splitName[1];
            $scope.last = splitName[0];
            $scope.hideAddRole = true;
        };

        $scope.updateResource = function () {
            var updatedResource = {
                Id: $scope.selectedResource.Id,
                FirstName: $scope.first,
                LastName: $scope.last,
                Roles: $scope.selectedResource.Roles,
                UserToTasks : $scope.selectedResource.UserToTasks
            }
            $scope.updatePercent();
            for (var i = 0; i < updatedResource.UserToTasks.length; i++) {
                console.log($scope.selectedTask.TaskId);
                console.log(updatedResource.UserToTasks[i].TaskId);
                if ($scope.selectedTask.TaskId == updatedResource.UserToTasks[i].TaskId) {
                    updatedResource.UserToTasks[i].Percentage = $scope.setPercentage;
                }
            }
            shared.updateResource(updatedResource);
            //clear form
            $scope.first = "";
            $scope.last = "";
            $scope.selectedResource = null;
            $scope.selectedTask = null;
        };

        $scope.addRoleToResource = function () {
            if ($scope.selectedResource.Roles.length != 0) {
                for (var i = 0; i < $scope.selectedResource.Roles.length; i++) {
                    if ($scope.selectedResource.Roles[i].Name != $scope.selectedRole.Name) {
                        //loop through and check for duplicates
                    } else { return }
                }
                //if no duplicates add role
                $scope.selectedResource.Roles.push($scope.selectedRole);
            }
        };

        $scope.updatePercent = function () {
            var percent = parseInt($scope.setPercentage);
            if (percent > 100)
                $scope.setPercentage = 100;
            else if(percent<0)
                $scope.setPercentage = 0;
            else
                $scope.setPercentage = percent;
        };
        
        //keep track of deleted items so they can be returned if modal is cancelled
        $scope.rolesToBeDeleted = [];

        $scope.removeRoleFromResource = function (role) {
            for (var i = 0; i < $scope.selectedResource.Roles.length; i++) {
                if ($scope.selectedResource.Roles.length <= 1)
                    window.alert("Users must have at least one role.")
                else if (role.Name == $scope.selectedResource.Roles[i].Name) {
                    $scope.rolesToBeDeleted.push(role);
                    $scope.selectedResource.Roles.splice(i, 1);
                }
            }
        };

        $scope.closeWithoutSaving = function () {
            //if user closes modal without saving re add any deleted roles
            if ($scope.rolesToBeDeleted.length != 0) {
                for (var i = 0; i < $scope.rolesToBeDeleted.length; i++) {
                    $scope.selectedResource.Roles.push($scope.rolesToBeDeleted[i]);
                }
                $scope.rolesToBeDeleted = [];
                //clear form
                $scope.first = "";
                $scope.last = "";
                $scope.selectedResource = null;
            }
            
            $scope.selectedResource = null;
            $scope.selectedTask = null;
            $scope.setPercentage = null;
            $scope.first = "";
            $scope.last = "";
        }
       
        $scope.addResource = function (resource) {
            if (resource.Roles.length != 0) {
                shared.addResource(resource);
                //reset form
                $scope.NewResourceFirstName = "";
                $scope.NewResourceLastName = "";
                $scope.selectedRoles = [];
                $scope.hideNewRoleField = false;
                $scope.newRoleButtonText = "Create Role";
            } else {
                console.log("Resource not saved because it has no roles associated with it")
            }
        }

        $scope.addNewRole = function () {
            if ($scope.hideNewRoleField && typeof $scope.newRoleName != 'undefined') {
                $scope.selectedRoles.push({ Name: $scope.newRoleName, Users: [] });
                console.log("Add new role to new resource");
                $scope.hideNewRoleField = !$scope.hideNewRoleField;
                $scope.newRoleButtonText = "Create Role";
                $scope.newRoleName = "";

            } else if(!$scope.hideNewRoleField) {
                console.log("else: ", $scope.selectedRole)
                $scope.selectedRoles.push($scope.selectedRole)
            }
        };

        $scope.updateNewRole = function () {
            if ($scope.hideNewRoleField && typeof $scope.newRoleName != 'undefined') {
                console.log("updateNewRoles");
                $scope.selectedResource.Roles.push({ Name: $scope.newRoleName, Users: [] });
                $scope.hideNewRoleField = !$scope.hideNewRoleField;
                $scope.newRoleButtonText = "Create Role";
                $scope.newRoleName = "";

            } else if (!$scope.hideNewRoleField) {
                console.log("else: ", $scope.selectedRole)
                $scope.selectedRoles.push($scope.selectedRole)
            }
        };

        $scope.newRoleButtonText = "Create Role";
        $scope.toggleNewRoleField = function () {
            $scope.hideNewRoleField = !$scope.hideNewRoleField;
            if ($scope.hideNewRoleField) {
                $scope.newRoleButtonText = "Back";
            } else {
                $scope.newRoleButtonText = "Create Role";
            }
        };
        /*------------Add/Manage Resources------------*/

        /*-------------Configure Preview-----------------*/
        $scope.isPreviewing = false; //determines if changes go to the database
        $scope.previewOnOrOff = { text: "off", btnclass: 'btn-warning' };
        $scope.previewmsg = {
            content: 'When preview mode is enabled, changes to the grantt chart will NOT be saved.',
            title: 'Preview Mode'
        };
        $scope.togglePreview = function () {
            if (!$scope.isPreviewing) {
                $scope.isPreviewing = true;
                shared.setPreview(true);
                $scope.previewOnOrOff.text = 'on';
            }
            else {
                $scope.isPreviewing = false;
                shared.setPreview(false);
                $scope.previewOnOrOff.text = 'off';
            }
        };
        /*-------------Configure Preview-----------------*/

        //Function to change the theme
        $scope.changeCSS = function(cssFile, cssLinkIndex) {

            var oldlink = document.getElementsByTagName("link").item(cssLinkIndex);
            var newlink = document.createElement("link");
            newlink.setAttribute("rel", "stylesheet");
            newlink.setAttribute("type", "text/css");
            newlink.setAttribute("href", cssFile);

            document.getElementsByTagName("head").item(0).replaceChild(newlink, oldlink);
        };

        //Function to hide DatePicker when in Timeline View
        $scope.hideDateField = true;
        $scope.$on('hideDateField', function (event, data) { $scope.hideDateField = data; });
        $scope.$on('showDateField', function (event, data) { $scope.hideDateField = data; });

        //datepicker options/////////////////
        $scope.open1 = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened1 = true;
        };
        $scope.open2 = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened2 = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        //Convert string obtained from input:date to date format
        $scope.$watch("Begin", function (newValue) {
            shared.setBeginView( new Date(newValue));
        });
        $scope.$watch("End", function (newValue) {
            shared.setEndView( new Date(newValue));
        });

        //initial dates in calendars
        $scope.Begin = new Date("01/1/2015");
        $scope.End = new Date("05/31/2015");

        $scope.debug = function () {
            console.log($scope.selectedResource);
        };

        $scope.updatePercentage = function (inputs) {
            var percent = 0;
            var task = shared.waitingToUpdate;
            task.UserToTasks = [];
            for (var i = 0; i < inputs.length; i++) {
                percent = parseInt(inputs[i].value);
                if (percent >= 0 && percent <= 100) { }
                else if (percent >= 100)
                    percent = 100;
                else
                    percent = 0;
                task.UserToTasks.push({ "UserId": inputs[i].id, "TaskId": task.id, "Percentage": percent, "Task": null, "User": null });
            }
            shared.updateTask(task);
        };

        $scope.addPercentage = function (inputs) {
            var percent = 0;
            var task = shared.waitingToAdd;
            task.UserToTasks = [];
            for (var i = 0; i < inputs.length; i++) {

                percent = parseInt(inputs[i].value);
                if (percent >= 0 && percent <= 100) { }
                else if (percent >= 100)
                    percent = 100;
                else
                    percent = 0;

                task.UserToTasks.push({ "UserId": parseInt(inputs[i].id), "TaskId": null, "Percentage": percent, "Task": null, "User": null });
                task.id = null;
            }
            shared.addTask(task);
        };

        $scope.getTaskName = function (id) {
            console.log(id);
            for (var i = 0; i < shared.tasks.length; i++) {
                if (shared.tasks[i].Id == id) {
                    return shared.tasks[i].Name;
                }
            }
            return null;
        }
    }
]);